# encoding=utf8
import requests
from bs4 import BeautifulSoup
import re
import json
import threading
import datetime
import sys

# server = http://173.235.74.153:8015/toner/
URL_SERVER = 'http://173.235.74.153:8015/toner/'
MIN_VALUE_PERCENT = 15  # bajo de este valor envia email

printer_version = ({
    'path': 'info_suppliesStatus.html?tab=Home&menu=SupplyStatus',
    'code': 'M570dn'
},
    {
    # 'hp/device/InternalPages/Index?id=SuppliesStatus',
    'path': 'hp/device/InternalPages/Index?id=SuppliesStatus',
    'code': 'M575'
})
printer_model = {
    'M570dn': 'info_suppliesStatus.html?tab=Home&menu=SupplyStatus',
    'M575': 'hp/device/InternalPages/Index?id=SuppliesStatus'
}
threads = []
subnets = []
printers = []
RETRY = 3  # intentos

# import urllib3
# urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def getSubnets():
    """Obtiene todas las subnet"""
    method = "get_all_subnet"
    response = requests.request("GET", URL_SERVER + method)
    for row in json.loads(response.text):
        subnets.append({
            'subnet_id': row['id'],
            'agency_id': row['agency_id'],
            'subnet': row['ip_adress'],
            'printers': []
        })
    response.close()
    # print(subnets)


def getIPPrinters():
    """Obtiene las ip de todas las impresoras"""
    method = 'get_all_printer'  # "get_ip_all_printer"
    response = requests.request("GET", URL_SERVER + method)
    for row in json.loads(response.text):
        printers.append(row)


# def get_valueOLD(text):
#     aux = text.replace(u"\xa0", u" ").replace('\\n', '')
#     return aux.encode('ascii', 'ignore').decode('utf-8').strip()


def get_value(text):
    # elimina espacios duplicados
    aux = " ".join(re.split(r"\s+", text, flags=re.UNICODE))
    aux = aux.split(u'\xa0')
    return ' '.join(c for c in aux if c != '').strip()


def __get_toner_id(toner_name):
    """Obtiene identificador del toner segun nombre"""
    toner_name = toner_name.lower()
    if 'black' in toner_name or 'negro' in toner_name:
        return 1
    elif 'yellow' in toner_name or 'amarillo' in toner_name:
        return 2
    elif 'magenta' in toner_name:
        return 3
    elif 'cyan' in toner_name or 'cian' in toner_name:
        return 4
    return None


# def __get_page(model, printer_ip):
#     """Captura la version de la impresora y el html"""
#     if model in printer_model:
#         part_path = printer_model[model]
#         page = requests.get(
#             'http://{0}/{1}'.format(printer_ip, v['path']), timeout=10)
#     result = None
#     for v in printer_version:
#         page = requests.get(
#             'http://{0}/{1}'.format(printer_ip, v['path']), timeout=10)
#         if page.status_code == 200:
#             result = {
#                 'version': v['code'],
#                 'page': page
#             }
#             break
#     return result


# en desuso
def __get_info_printerOLD(subnet, printer_ip):
    """Captura los valores de tinta"""
    try:  # 10.32.10.16
        page = requests.get(
            "http://{0}/hp/device/info_suppliesStatus.html?tab=Home&amp;menu=SupplyStatus".format(printer_ip), timeout=2)
        # "http://{0}:8080/test.html".format(printer_ip), timeout=1)  # test
        status_code = page.status_code
    except Exception:
        status_code = 0
    if status_code == 200:
        soup = BeautifulSoup(page.content, 'html.parser')
        table = soup.find_all('table', 'width100')[0]
        toner_low = []
        printer_description = get_value(soup.find_all('div', 'userId')[
            0].get_text())
        # recorre cada color
        for row in table.find_all('table', 'mainContentArea')[:-1]:
            value_name = get_value(row.find_all('td', 'SupplyName width65')[
                0].get_text())  # nombre
            value_percent = get_value(row.find_all('td', 'SupplyName width35 alignRight')[
                0].get_text())  # porcentaje
            values = row.find_all('td', 'itemSpsFont')
            value_printed_pages = 0
            serial_number = None
            if values:
                value_state = values[0].get_text()  # estado
                if len(values) > 1:  # valor ademas del status
                    # paginas impresas
                    value_printed_pages = get_value(values[2].get_text())
                    # captura numero serial
                    serial_number = get_value(values[3].get_text())
            # porcentaje numero
            if '--%' in value_percent:
                value_percent = '0%'
                percent = 0
            else:
                aux = re.search(r'\d+(?=%)', value_percent)
                if aux:
                    percent = aux.group(0)
                else:
                    value_percent = percent = 0
            toner_low.append({
                'toner_id': __get_toner_id(value_name),
                'percent': percent,
                'serial_number': serial_number,
                'page_printed': value_printed_pages
            })
            # print('printed: ' + str(value_printed_pages))
            subnet['printers'].append({
                'ip_adress': printer_ip,
                'name': printer_description,
                'toners': toner_low
            })
        # print("{0} end scan".format(printer_ip))


def __get_info_m570dn(soup, printer_ip):
    """Scrapping impresora modelo HP LaserJet 500 colorMFP M570dn"""
    print('V1 M570dn  :: ', printer_ip)
    aux = soup.find('div', 'userId')  # busca la primera etiqueta
    if aux:  # de no existir la primera etiqueta no es la impresora adecuada
        result = {
            'name': get_value(aux.string),
            'toners': []
        }
        infoArea = soup.find('td', {'class': 'rightContentPane'}).extract()
        for item in infoArea.find_all('table', {'class': 'mainContentArea'})[:-1]:
            tds = item.find_all('td', {'class': 'itemSpsFont'})
            toner_info_header = item.find('table', {'class': 'width100'})
            tds_info_header = toner_info_header.find_all(
                'td', {'class': ['SupplyName', 'width35']})
            percent = re.findall(
                r'\d+', get_value(tds_info_header[1].get_text()))
            toner = {
                'toner_id': __get_toner_id(tds_info_header[0].get_text()),
                'printed_pages': None,
                'serial_number':  None,
                'percent': percent[0] if len(percent) > 0 else 0,
                'unrecognized': False
            }
            if len(tds) > 2:  # si existe la informacion del toner
                toner['printed_pages'] = get_value(tds[2].get_text())
                toner['serial_number'] = get_value(tds[3].get_text())
            else:  # de no existir la información del toner
                toner['unrecognized'] = True
            result['toners'].append(toner)
        return result
    return None


def __get_info_m575(soup, printer_ip):
    """Scrapping impresora modelo HP LaserJet 500 color MFP M575"""
    print('V2 M757 :: ', printer_ip)
    divInfo = soup.find('div', 'info')
    if divInfo:
        description = '{0} {1}'.format(
            divInfo.find('p', {'class': 'device-name'}).string, divInfo.find('p', {'class': 'ip-address'}).string)
        result = {
            'name': description,
            'toners': []
        }
        pageContent = soup.find('div', {'id': 'InternalPageContent'})
        divToners = pageContent.find_all(
            'div', {'class': 'content-columns'})[1]
        for item in divToners.find_all('div', {'class': 'section-content'})[:-4]:
            percent = re.findall(r'\d+',
                                 get_value(item.find('p', {'class': 'data percentage'}).get_text()))
            aux_p = item.find_all('p', {'class': 'data'})
            toner = {
                'toner_id': __get_toner_id(item.find('h2').get_text()),
                'serial_number': None,
                'printed_pages': None,
                'percent': percent[0] if len(percent) > 0 else 0,
                'unrecognized': False
            }
            if len(aux_p) > 3:  # si existe la informacion (PRUEBA)
                toner['serial_number'] = get_value(
                    aux_p[4].find('strong').get_text())
                toner['printed_pages'] = get_value(
                    aux_p[5].find('strong').get_text())
            else:  # de no existir la información del toner
                toner['unrecognized'] = True
            result['toners'].append(toner)
        return result
    return None


def __get_info_printer(subnet, printer_info):
    """Obtiene la informacion de la impresora según el modelo"""
    page = None
    if printer_info['model'] in printer_model:  # si el modelo es soportado
        for tried in range(1, RETRY):  # intentos
            try:
                req = requests.get(
                    'http://{0}/{1}'.format(printer_info['ip_adress'], printer_model[printer_info['model']]), timeout=10)
                if req.status_code == 200:
                    page = req
                break
            except (requests.ConnectTimeout, requests.ConnectionError):
                print('Error timeout:', printer_info['ip_adress'])
                print('Tried', tried + 1, printer_info['ip_adress'])
        if page:
            printer = None
            soup = BeautifulSoup(page.content,
                                 'lxml', from_encoding='utf-8')
            try:
                if printer_info['model'] == 'M570dn':
                    printer = __get_info_m570dn(
                        soup, printer_info['ip_adress'])
                elif printer_info['model'] == 'M575':
                    printer = __get_info_m575(soup, printer_info['ip_adress'])
            except (Exception) as e:
                print('Error printer ' + printer_info['ip_adress'], e)
            if printer:
                printer['ip_adress'] = printer_info['ip_adress']
                printer['agency_id'] = printer_info['agency_id']
                subnet['printers'].append(printer)
    else:
        print('Printer ', printer_info['model'],
              printer_info['ip_adress'], ' not supported')


def send_update(obj):
    """Envia la informacion a la api"""
    method = "update_state_printer"
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache"
    }
    response = requests.request(
        "POST", URL_SERVER + method, data=json.dumps(obj), headers=headers)
    print(response.text)
    response.close()


printers2 = []


def version1():
    getSubnets()  # obtiene las subnet
    #subnets[0]['subnet'] = '10.32.10'
    for subnet in subnets:   # recorre las subnet
        for i in range(1, 255):
            ip = '{0}.{1}'.format(subnet['subnet'], i)  # crea ip
            # ip = "10.32.10.17"  # 10.1.2.65 10.6.2.200
            tr = threading.Thread(target=__get_info_printer,
                                  args=(subnet, ip))
            threads.append(tr)
            tr.start()
            #__get_info_printer(subnet, ip)
    # espera todos los hilos
    print('esperando threads ', len(threads))
    for thread in threads:
        thread.join()
    print('enviando resultados')
    printer_total = 0
    for subnet in subnets:
        printer_total += len(subnet['printers'])
        for printer in subnet['printers']:
            printers2.append(printer['ip_adress'])
            continue
            # send_update({
            #     'subnet_id': subnet['subnet_id'],
            #     'agency_id': subnet['agency_id'], c
            #     'name': printer['name'],
            #     'ip_adress': printer['ip_adress'],
            #     'toners': printer['toners']
            # })
    print(printer_total, ' impresoras encontradas')
    print('terminado')


def version2():
    getIPPrinters()  # obtiene todas las impresoras
    subnet = {
        'printers': []
    }
    for printer_info in printers:
        tr = threading.Thread(target=__get_info_printer,
                              args=(subnet, printer_info))
        threads.append(tr)
        tr.start()
    print('esperando threads')
    for thread in threads:
        thread.join()
    print(len(subnet['printers']), ' impresoras encontradas')
    print('enviando resultados')
    for printer in subnet['printers']:
        send_update(printer)
    print('terminado')


version2()

# print('comprobacion', len(printers2))
# aux = [
#     '10.1.2.148',
#     '10.1.2.60',
#     '10.1.2.62',
#     '10.1.2.51',
#     '10.1.2.178',
#     '10.1.2.181',
#     '10.1.2.56',
#     '10.1.2.52',
#     '10.1.2.53',
#     '10.1.2.65',
#     '10.1.2.58',
#     '10.2.1.52',
#     '10.2.1.53',
#     '10.2.1.94',
#     '10.3.2.53',
#     '10.4.2.150',
#     '10.3.2.54',
#     '10.3.2.56',
#     '10.3.2.51',
#     '10.6.2.138',
#     '10.6.2.52',
#     '10.6.2.60',
#     '10.6.2.57',
#     '10.6.2.54',
#     '10.6.2.50',
#     '10.6.2.51',
#     '10.6.2.200',
#     '10.6.2.53',
#     '10.6.2.98',
#     '10.6.2.61',
#     '10.6.2.66'
# ]
# print('impresoras encontradas')
# for ip in printers2:
#     print(ip)
# print('impresoras no encontradas')
# for ip in aux:
#     if ip not in printers2:
#         print(ip)
