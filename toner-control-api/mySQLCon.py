import mysql.connector
from flask import jsonify


class my_row:

    def __init__(self, cursor, row):
        self.cursor = cursor
        self.row = row

    def get_value(self, column_name, default_value=None):
        return self.row[column_name] or default_value


class reader_row_by_row:

    def __init__(self, cursor):
        self.cursor = cursor

    def __enter__(self):
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.close()


class RunAndRead:

    def __init__(self, my_con):
        self.my_con = my_con
        try:
            self.cursor = my_con.cnx.cursor(dictionary=True)
            self.cursor.execute(self.my_con.Command, self.my_con.Params)
        except mysql.connector.Error as err:
            self.my_con.cnx.rollback()  # rollback a todo
            raise err

    def __enter__(self):
        return self.cursor

    def __aiter__(self):
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        if self.cursor:
            self.cursor.close()

class MSQLTrans:

    def __init__(self):
        self.cnx = mysql_con()

    def __enter__(self):
        self.cnx.cnx.start_transaction()
        return self.cnx

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.cnx.cnx.in_transaction:
            self.cnx.cnx.commit()
        self.cnx.con_close()
        

class mysql_con:

    def __new_conn(self):
        config = {
            'user': 'root',  # 'jeff',
            'password': 'N1c4r4gu4',  # 'aguilar08',
            'host': 'localhost',  # '173.235.74.153',
            'database': 'dbtonnercontrol',
            'port': 3306,
            'raise_on_warnings': True
        }
        self.cnx = mysql.connector.connect(**config, buffered=True)
        # self.cnx.start_transaction()
        return self.cnx

    def con_close(self):
        print('cierra coneccion')
        self.cnx.close()

    def __init__(self):
        self.cnx = None
        self.Command = str
        self.Params = []
        self.__new_conn()

    def __enter__(self):
        return self

    def execute_non_query(self):
        """Ejecuta la sintaxis"""
        RunAndRead(self).close()

    def insert(self) -> object:
        """Inserta item y develve el id de la inserción"""
        with RunAndRead(self) as cursor:
            return cursor.lastrowid

    def reader(self, func):
        aux = []
        with RunAndRead(self) as cursor:
            for row in cursor:
                aux.append(func(my_row(cursor, row)))
        return aux

    def execute_get_column(self, name_column) -> object:
        """Ejecuta la consulta y devuelve el valor de la columna de la primera fila obtenida"""
        with RunAndRead(self) as cursor:
            for row in cursor:
                return row[name_column]
        return None

    def get_reader(self) -> reader_row_by_row:
        return reader_row_by_row(RunAndRead(self).cursor)

    def __exit__(self, type, value, tb):
        # self.cnx.commit()
        self.con_close()
