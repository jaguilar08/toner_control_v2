from flask import Flask, jsonify, request, json, make_response
from mySQLCon import mysql_con, MSQLTrans
from flask_cors import CORS
from DGAPI import DGAPI
from functools import wraps
import math
from itertools import groupby
from send_email import send_email
from const import DEBUG, CCS
# import cgitb
# cgitb.enable()

app = Flask(__name__)
CORS(app)

dg = DGAPI()


def login_required(f):
    """Decorador"""

    @wraps(f)
    def decorated(*args, **kwargs):
        if is_authorize():
            return f(*args, **kwargs)
        else:
            return make_response(jsonify({'error': 'Unauthorized access'}), 403)

    return decorated


def is_authorize():
    return DGAPI().get('valid_token')["status"] != 403


def success_ok():
    return make_response(jsonify({"message": "success"}), 200)


@app.route('/', methods=['GET'])
def hello():
    with mysql_con():
        pass
    return jsonify({
        "message": "hello world"
    })


@app.route('/authentication', methods=['POST'])
def authentication():
    json_obj = json.loads(request.data.decode('utf-8'))
    json_param = {
        "application_id": 3,
        "device_id": json_obj['device_id'],
        "password": json_obj['password'],
        "username": json_obj['username']
    }
    result = dg.post("Authentication", json_param)
    response = json.loads(result['response'])
    if result['status'] == 200:
        duser_json = response['d_user']
        user_application_id = duser_json['user_application_id']
        with mysql_con() as cnx:
            cnx.Command = "select id from toner_user " \
                "where user_application_id = %s;"
            cnx.Params = (user_application_id,)
            duser_json['toner_user_id'] = cnx.execute_get_column('id')
    return make_response(jsonify(response), result['status'])


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    result = dg.get("logout")
    return make_response(result['response'], result['status'])


@app.route('/get_home', methods=['POST'])
@login_required
def get_home():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select printer.id, name, printer.ip_adress, printer_model.model, last_update from printer
        inner join printer_model on printer.printer_model_id = printer_model.id
        where subnet_id = %s and active = 1;
        """
        cnx.Params = (json_obj['subnet_id'], )
        with cnx.get_reader() as reader:
            for row in reader:
                printer = {
                    "name": row['name'],
                    "model": row['model'],
                    "ip_adress": row['ip_adress'],
                    "last_update": __datetime_format(row['last_update']),
                    "toners": []
                }
                cnx.Command = """\
                select
                toner.name as 'toner',
                toner.font_color,
                toner.color_hex,
                infoIns.serial_number,
                infoIns.percent,
                infoIns.page_printed,
                toner_using.isWaitingForChange
                from toner_using
                inner join toner on toner_using.toner_id = toner.id
                left join toner_info infoIns on toner_using.toner_info_installed_id = infoIns.id
                where printer_id = %s
                order by toner.id asc;
                """
                cnx.Params = (row['id'], )
                with cnx.get_reader() as reader_toner:
                    for row_toner in reader_toner:
                        printer['toners'].append({
                            'toner': row_toner['toner'],
                            'font_color': row_toner['font_color'],
                            'color_hex': row_toner['color_hex'],
                            'serial_number': row_toner['serial_number'] or None,
                            'page_printed': row_toner['page_printed'],
                            'percent': row_toner['percent'] or 0,
                            'isWaitingForChange': bool(row_toner['isWaitingForChange'])
                        })
                result.append(printer)
    return jsonify(result)


@app.route('/get_home_resume', methods=['GET'])
@login_required
def get_home_resume():
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select id, name from subnet
        order by subnet.id
        """
        cnx.Params = None
        with cnx.get_reader() as reader_subnet:
            for r_subnet in reader_subnet:
                aux_subnet = {
                    "id": r_subnet['id'],
                    "name": r_subnet['name'],
                    "printers": []
                }
                cnx.Command = """\
                select printer.id, name, printer.ip_adress, printer_model.model, last_update from printer
                inner join printer_model on printer.printer_model_id = printer_model.id
                where subnet_id = %s and active = 1;
                """
                cnx.Params = (r_subnet['id'], )
                with cnx.get_reader() as reader:
                    for row in reader:
                        printer = {
                            "name": row['name'],
                            "model": row['model'],
                            "ip_adress": row['ip_adress'],
                            "last_update": __datetime_format(row['last_update']),
                            "toners": []
                        }
                        cnx.Command = """\
                        select
                        toner.name as 'toner',
                        toner.font_color,
                        toner.color_hex,
                        infoIns.serial_number,
                        infoIns.percent,
                        infoIns.page_printed,
                        toner_using.isWaitingForChange
                        from toner_using
                        inner join toner on toner_using.toner_id = toner.id
                        left join toner_info infoIns on toner_using.toner_info_installed_id = infoIns.id
                        where printer_id = %s and (infoIns.percent < 15 or toner_using.toner_info_installed_id is null)
                        order by toner.id asc;
                        """
                        cnx.Params = (row['id'], )
                        with cnx.get_reader() as reader_toner:
                            for row_toner in reader_toner:
                                printer['toners'].append({
                                    'toner': row_toner['toner'],
                                    'font_color': row_toner['font_color'],
                                    'color_hex': row_toner['color_hex'],
                                    'serial_number': row_toner['serial_number'] or None,
                                    'page_printed': row_toner['page_printed'],
                                    'percent': row_toner['percent'] or 0,
                                    'isWaitingForChange': bool(row_toner['isWaitingForChange'])
                                })
                        if len(printer['toners']) > 0:
                            aux_subnet['printers'].append(printer)
                if len(aux_subnet['printers']) > 0:
                    result.append(aux_subnet)
    return jsonify(result)


@app.route('/get_toners', methods=['GET'])
@login_required
def get_toners():
    result = []
    with mysql_con() as cnx:
        cnx.Command = "select * from toner;"
        cnx.Params = None
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    'id': row['id'],
                    'name': row['name'],
                    'color_hex': row['color_hex'],
                    'font_color': row['font_color'],
                    'quantity': row['quantity']
                })
    return jsonify(result)


# inserta impresora
def __insert_printer(cnx, json_obj) -> int:
    """Inserta impresora y devuelve el identificador de esta"""
    cnx.Command = """
        select printer.id from printer
        where printer.ip_adress = %s;
        """
    cnx.Params = (json_obj['ip_adress'],)
    printer_id = cnx.execute_get_column('id')
    # if not printer_id:
    #     cnx.Command = """\
    #     insert into printer (name, subnet_id, ip_adress)
    #     values (%s, %s, %s);
    #     """
    #     cnx.Params = (json_obj['name'],
    #                   json_obj['subnet_id'], json_obj['ip_adress'])
    #     printer_id = cnx.insert()
    # else:  # actualiza nombre de imporesora
    cnx.Command = """\
    update printer
    set name = %s,
    last_update = UTC_TIMESTAMP()
    where ip_adress = %s;
    """
    cnx.Params = (json_obj['name'], json_obj['ip_adress'])
    cnx.execute_non_query()
    return printer_id


# actualiza la informacion del toner
def __update_toner_info(cnx, toner) -> int:
    """Inserta información de los toner"""
    cnx.Command = """\
    select id from toner_info
    where toner_info.serial_number = %s;
    """
    cnx.Params = (toner['serial_number'],)
    toner_info_id = cnx.execute_get_column('id')
    if toner_info_id:
        cnx.Command = """\
        update toner_info set
        percent = %s,
        page_printed = %s,
        last_update = UTC_TIMESTAMP()
        where toner_info.id = %s;
        """
        cnx.Params = (toner['percent'],
                      toner['printed_pages'], toner_info_id)
        cnx.execute_non_query()
    else:
        cnx.Command = """\
                insert into toner_info (toner_id, serial_number, percent, page_printed, last_update)
                values (%s, %s, %s, %s, UTC_TIMESTAMP());
                """
        cnx.Params = (toner['toner_id'], toner['serial_number'],
                      toner['percent'], toner['printed_pages'])
        toner_info_id = cnx.insert()
    return toner_info_id


# registra toner para ser retirado por dealergeek
def __residual_toner(cnx, agency_id, printer_id, toner_id, old_toner_info_id):
    """Inserta registro de toner vacio"""
    if old_toner_info_id:  # si existe un toner antes
        cnx.Command = """\
        select id from toner_inventory
        where agency_id = %s and toner_id = %s;
        """
        cnx.Params = (agency_id, toner_id)
        toner_inventory_id = cnx.execute_get_column('id')
        if toner_inventory_id:
            cnx.Command = """\
            update toner_inventory set
            residual_quantity = residual_quantity + 1
            where id %s;
            """
            cnx.Params = (toner_inventory_id, )
        else:
            cnx.Command = """\
            insert into toner_inventory(agency_id, toner_id, residual_quantity)
            values (%s, %s, 1);
            """
            cnx.Params = (agency_id, toner_id)
            toner_inventory_id = cnx.insert()
        cnx.Command = """\
        insert into toner_inventory_residual(toner_inventory_id, printer_id, toner_info_id, date_post)
        values (%s, %s, %s, UTC_TIMESTAMP());
        """
        cnx.Params = (toner_inventory_id, printer_id, old_toner_info_id)
        cnx.execute_non_query()


def __change_of_toner(cnx, exists_toner, printer_id, agency_id, toner_id, toner_info_id, percent_toner) -> int:
    """Toner en uso para la imporesora"""
    toner_using_id = None
    current_toner_info_id = None
    # obtiene el toner instalado en la impresora
    cnx.Command = """
    select id, 
    ifnull(toner_info_installed_id, 0) as 'toner_info_installed_id'
    from toner_using
    where printer_id = %s and toner_id = %s;
    """
    cnx.Params = (printer_id, toner_id)
    with cnx.get_reader() as reader:
        for row in reader:
            toner_using_id = row['id']
            current_toner_info_id = row['toner_info_installed_id']
    # si hay un cambio de toner
    if current_toner_info_id != toner_info_id:
        # reemplaza el toner
        cnx.Command = """
        update toner_using
        set toner_info_installed_id = %s,
        isWaitingForChange = 0
        where id = %s;
        """
        cnx.Params = (toner_info_id, toner_using_id)
        cnx.execute_non_query()
        # instalacion de nuevo toner
        cnx.Command = """
        insert into installed_toner(printer_id, toner_info_id, instalation_date)
        values (%s, %s, UTC_TIMESTAMP());
        """
        cnx.Params = (printer_id, toner_info_id)
        cnx.insert()
        # si el toner instalado es nuevo
        if not exists_toner:
            # asigna el toner nuevo a la impresora que corresponde
            cnx.Command = """
            update detail_toner_sent as detail set
            toner_assignment_info_id = %s
            where detail.printer_id = %s 
            and detail.toner_assignment_info_id is null and detail.toner_id = %s;
            """
            cnx.Params = (toner_info_id, printer_id, toner_id)
            cnx.execute_non_query()
            # afecta inventario
            if percent_toner > 95:  # si tiene mas del 95% es nuevo
                cnx.Command = """
                CALL proc_installed_toner(%s, %s);
                """
                #cnx.Params = (toner['toner_id'], json_obj['agency_id'])
                cnx.Params = (toner_id, agency_id)
                cnx.execute_non_query()
        # si ya existia un toner instalado antes del cambio
        if current_toner_info_id > 0:
            # registra toner por retirar
            __residual_toner(cnx, agency_id,
                             printer_id, toner_id, current_toner_info_id)
    return current_toner_info_id


@app.route('/update_state_printer', methods=['POST'])
# @login_required
def update_state_printer():
    json_obj = json.loads(request.data.decode('utf-8'))
    with MSQLTrans() as cnx:
        printer_id = __insert_printer(cnx, json_obj)
        for toner in json_obj['toners']:
            # cuando no se pudo reconocer el toner
            if toner['unrecognized'] or not toner['serial_number']:
                cnx.Command = """
                update toner_using set toner_info_installed_id = null
                where printer_id = %s and toner_id = %s;
                """
                cnx.Params = (printer_id, toner['toner_id'])
                cnx.execute_non_query()
            else:
                # verificando si el toner ya estaba registrado, de lo contrario es nuevo
                cnx.Command = """\
                select existToner(%s) as 'exists';
                """
                cnx.Params = (toner['serial_number'],)
                exists = bool(cnx.execute_get_column('exists'))
                # actualiza los datos del toner
                toner_info_id = __update_toner_info(cnx, toner)
                # actualiza el toner instalado en la impresora
                __change_of_toner(cnx, exists, printer_id,
                                  json_obj['agency_id'], toner['toner_id'], toner_info_id,
                                  int(toner['percent']))
    return success_ok()


@app.route('/add_order', methods=['POST'])
@login_required
def add_order():
    json_obj = json.loads(request.data.decode('utf-8'))
    with MSQLTrans() as cnx:
        cnx.Command = """\
        insert into toner_order (toner_user_id, number, order_date, post_date)
        values (%s, %s, %s, UTC_TIMESTAMP());
        """
        cnx.Params = (json_obj['toner_user_id'],
                      json_obj['number'], json_obj['order_date'])
        toner_order_id = cnx.insert()
        for item in json_obj['toners']:
            cnx.Command = """\
            insert into detail_toner_order (toner_order_id, toner_id, quantity, pending_quantity)
            values (%s, %s, %s, %s);
            """
            cnx.Params = (toner_order_id,
                          item['id'], item['quantity'], item['quantity'])
            cnx.execute_non_query()
    return make_response(jsonify({"message": "success"}), 201)


@app.route('/get_order_by_number', methods=['GET'])
@login_required
def get_order_by_number():
    order_number = int(request.args.get('order_number'))
    result = {
        "id": 0,
        "number": order_number,
        "user": None,
        "order_date": None,
        "post_date": None,
        "toners": []
    }
    with mysql_con() as cnx:
        cnx.Command = """\
        select toner_order.*,
        toner_user_view.name as 'user'
        from toner_order
        INNER JOIN toner_user_view on toner_order.toner_user_id = toner_user_view.id
        where number = %s;
        """
        cnx.Params = (order_number, )
        with cnx.get_reader() as reader:
            for row in reader:
                result['id'] = row['id']
                result['user'] = row['user']
                result['order_date'] = __date_format(row['order_date'])
                result['post_date'] = __datetime_format(row['post_date'])
        cnx.Command = """\
        select detail.id, detail.quantity,
        detail.pending_quantity,
        toner.name as 'toner'
        from detail_toner_order detail
        INNER JOIN toner on detail.toner_id = toner.id
        where detail.toner_order_id = %s
        order by toner.id;
        """
        cnx.Params = (result['id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result['toners'].append({
                    "detail_toner_order_id": row['id'],
                    "toner": row['toner'],
                    "quantity": row['quantity'],
                    "pending_quantity": row['pending_quantity']
                })
    return jsonify(result)


@app.route('/get_last_order', methods=['POST'])
@login_required
def get_last_order():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    with mysql_con() as cnx:
        # indica si solo carga los pendiente de verificacion
        if json_obj['load_pending_verified'] and json_obj['load_pending_verified'] != -1:
            cnx.Command = """
            select toner_order.*,
            tuser.name as 'user'
            from toner_order
            INNER JOIN toner_user_view tuser on toner_order.toner_user_id = tuser.id
            where toner_order.pending_verified = 1;
            """
            cnx.Params = None
        else:
            cnx.Command = """\
            select toner_order.*,
            tuser.name as 'user'
            from toner_order
            INNER JOIN toner_user_view tuser on toner_order.toner_user_id = tuser.id
            where if(%s = 0, 1, toner_order.number = %s)
            and toner_order.order_date between %s and %s
            and if(%s = -1, 1, toner_order.pending_verified = %s)
            order by post_date desc
            """
            cnx.Params = (json_obj['order_number'], json_obj['order_number'],
                          json_obj['start_date'], json_obj['end_date'],
                          json_obj['load_pending_verified'], json_obj['load_pending_verified'])
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "id": row['id'],
                    'number': row['number'],
                    'user': row['user'],
                    'order_date': __date_format(row['order_date']),
                    'post_date': __date_format(row['post_date']),
                    'pending_verified': bool(row['pending_verified']),
                    'state': "Pending" if bool(row['pending_verified']) else "Verified"
                })
    return jsonify(result)


@app.route('/verified_order', methods=['POST'])
@login_required
def verified_order():
    json_obj = json.loads(request.data.decode('utf-8'))
    with MSQLTrans() as cnx:
        # inserta verificacion
        cnx.Command = """
        insert into toner_order_verified(toner_user_id, toner_order_id, post_date)
        values (%s, %s, UTC_TIMESTAMP());
        """
        cnx.Params = (json_obj['toner_user_id'], json_obj['toner_order_id'])
        toner_order_verified_id = cnx.insert()
        sEmail = False
        for row in json_obj['toners']:
            row['back_quantity'] = row['pending_quantity'] - \
                row['received_quantity']
            if not sEmail:
                sEmail = row['back_quantity'] > 0
            # actualiza los valores de la orden
            cnx.Command = """
            update detail_toner_order
            set pending_quantity = pending_quantity - %s
            where id = %s;
            """
            cnx.Params = (row['received_quantity'],
                          row['detail_toner_order_id'])
            cnx.execute_non_query()
            # inserta en detalle
            cnx.Command = """
            insert into detail_toner_order_verified (toner_order_verified_id, detail_toner_order_id,
            before_quantity, received_quantity)
            values (%s, %s, %s, %s);
            """
            cnx.Params = (toner_order_verified_id, row['detail_toner_order_id'],
                          row['pending_quantity'], row['received_quantity'])
            cnx.execute_non_query()
        cnx.Command = "CALL proc_update_pending_order(%s);"
        cnx.Params = (json_obj['toner_order_id'],)
        cnx.execute_non_query()
        if sEmail:  # si se envia reporte
            __send_email_verified_order(json_obj)
    return success_ok()


def __send_email_verified_order(data):
    """Envia via correo que la orden está incompleta"""
    aux = ""
    for row in data['toners']:
        aux = aux + """
        <tr>
            <td scope="row">{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
            <td>{4}</td>
        </tr>
        """.format(
            row['toner'],
            row['quantity'],
            row['pending_quantity'],
            row['received_quantity'],
            row['back_quantity']
        )
    style = """\
    <style>
        .subnet {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .subnet td, .subnet th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .subnet tr:nth-child(even){background-color: #f2f2f2;}

        .subnet tr:hover {background-color: #ddd;}

        .subnet th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4c61af;
            color: white;
        }
        </style>
    """
    doc_html = """
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        {0}
    </head>
    <body>
        <div>
            <h3>Not match</h3>
            <div>
                <b>Order number:</b> <small>{1}</small>
            </div>
            <br />
            <br />
            <div class="table-responsive">
                <table class="subnet">
                    <thead>
                        <tr>
                            <th scope="col">Description</th>
                            <th scope="col">Qty ordered</th>
                            <th scope="col">Qty pending</th>
                            <th scope="col">Qty received</th>
                            <th scope="col">Back ordered</th>
                        </tr>
                    </thead>
                    <tbody>
                        {2}
                    </tbody>
                </table>
            </div>
        </div>
    </body>
    </html>
    """.format(
        style,
        data['toner_order_number'],
        aux
    )
    nEmail = {
        'subject': 'Toner control system - Order number {0} incomplete'.format(data['toner_order_number']),
        'ccs': CCS,
        'body': doc_html
    }
    send_email(nEmail)


@app.route('/get_all_verified_order', methods=['POST'])
@login_required
def get_all_verified_order():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = {
        'pending_verified': True,
        'list': []
    }
    with mysql_con() as cnx:
        cnx.Command = """
        select toner_order.id, toner_order.pending_verified from toner_order
        where number = %s;
        """
        cnx.Params = (json_obj['order_number'],)
        with cnx.get_reader() as reader:
            for row in reader:
                json_obj['toner_order_id'] = row['id']
                result['toner_order_id'] = json_obj['toner_order_id']
                result['pending_verified'] = bool(row['pending_verified'])
        result['pending_verified'] = bool(
            cnx.execute_get_column('pending_verified'))
        print(result)
        # obtiene la lista
        cnx.Command = """
        select tverified.*, tuser.name from toner_order_verified tverified
        INNER JOIN toner_user_view tuser on tverified.toner_user_id = tuser.id
        where tverified.toner_order_id = %s;
        """
        cnx.Params = (json_obj['toner_order_id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result['list'].append({
                    "id": row['id'],
                    "user": row['name'],
                    "post_date": __datetime_format(row['post_date'])
                })
    return jsonify(result)


@app.route('/get_detail_verified_order', methods=['POST'])
@login_required
def get_detail_verified_order():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = {
        "id": json_obj['toner_order_verified_id'],
        "details": []
    }
    with mysql_con() as cnx:
        # Datos generales
        cnx.Command = """
        select tverified.*, tuser.name, toner_order.number from toner_order_verified tverified
        INNER JOIN toner_order on tverified.toner_order_id = toner_order.id
        INNER JOIN toner_user_view tuser on tverified.toner_user_id = tuser.id
        where tverified.id = %s;
        """
        cnx.Params = (json_obj['toner_order_verified_id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result['toner_order_number'] = row['number']
                result['toner_order_id'] = row['toner_order_id']
                result['user'] = row['name']
                result['post_date'] = __date_format(row['post_date'])
        # detalle
        cnx.Command = """
        select
        toner.name,
        dorder.quantity,
        detail.before_quantity,
        detail.received_quantity,
        detail.after_quantity
        from detail_toner_order_verified detail
        inner join detail_toner_order dorder on detail.detail_toner_order_id = dorder.id
        inner join toner on dorder.toner_id = toner.id
        where detail.toner_order_verified_id = %s;
        """
        cnx.Params = (json_obj['toner_order_verified_id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result['details'].append({
                    "toner": row['name'],
                    "quantity": row['quantity'],
                    "before_quantity": row['before_quantity'],
                    "received_quantity": row['received_quantity'],
                    "after_quantity": row['after_quantity']
                })
    return jsonify(result)


@app.route('/get_all_agency', methods=['GET'])
@login_required
def get_all_agency():
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select * from agency
        """
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "id": row['id'],
                    "name": row['name']
                })
    return jsonify(result)


@app.route('/get_all_subnet', methods=['GET'])
def get_all_subnet():
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select * from subnet
        """
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "id": row['id'],
                    "agency_id": row['agency_id'],
                    "name": row['name'],
                    "ip_adress": row['ip_adress']
                })
    return jsonify(result)


@app.route('/get_all_printer', methods=['GET'])
def get_all_printer():
    """Obtiene las todas las impresoras"""
    result = []
    with mysql_con() as cnx:
        cnx.Command = """
        select printer.ip_adress, model, subnet.agency_id from printer
        inner join printer_model on printer.printer_model_id = printer_model.id
        inner join subnet on printer.subnet_id = subnet.id
        """
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    'ip_adress': row['ip_adress'],
                    'agency_id': row['agency_id'],
                    'model': row['model']
                })
    return jsonify(result)


@app.route('/get_printers_for_agency', methods=['GET'])
@login_required
def get_printers_for_agency():
    agency_id = int(request.args.get('agency_id'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select printer.*,
        subnet.name as 'subnet'
        from printer
        inner join subnet on printer.subnet_id = subnet.id
        where subnet.agency_id = %s
        order by subnet.name;
        """
        cnx.Params = (agency_id, )
        aux = []
        with cnx.get_reader() as reader:
            for row in reader:
                aux.append({
                    "id": row['id'],
                    "subnet_id": row['subnet_id'],
                    "subnet": row['subnet'],
                    "name": row['name']
                })
        for id, printers in groupby(aux, key=lambda x: x['subnet_id']):
            subnet = {
                'name': None,
                'printers': []
            }
            for printer in printers:
                subnet['name'] = printer['subnet']
                subnet['printers'].append({
                    "id": printer['id'],
                    "name": printer['name'],
                })
            result.append(subnet)
    return jsonify(result)


@app.route('/get_printers_for_subnet', methods=['GET'])
@login_required
def get_printers_for_subnet():
    subnet_id = int(request.args.get('subnet_id'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """
        select printer.id, printer.name from printer
        where printer.subnet_id = %s;
        """
        cnx.Params = (subnet_id,)
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "id": row['id'],
                    "name": row['name']
                })
    return jsonify(result)


def __inventory_update(cnx, agency_id, toner_id, backup_quantity):
    """Afecta invetario agencia y dealegeek"""
    cnx.Command = """\
    select id from toner_inventory
    where agency_id = %s and toner_id = %s;
    """
    cnx.Params = (agency_id, toner_id)
    toner_inventory_id = cnx.execute_get_column('id')
    cant = 0 if backup_quantity > 0 else 1
    if toner_inventory_id:
        cnx.Command = """\
        update toner_inventory set
        quantity = quantity + %s,
        backup_quantity = backup_quantity + %s
        where id = %s;
        """
        cnx.Params = (cant, backup_quantity, toner_inventory_id)
        cnx.execute_non_query()
    else:
        cnx.Command = """\
        insert into toner_inventory (agency_id, toner_id, quantity, backup_quantity)
        values (%s, %s, %s, %s);
        """
        cnx.Params = (agency_id, toner_id, cant, backup_quantity)
        toner_inventory_id = cnx.insert()
    # afecta inventario dealergeek
    cnx.Command = """\
    update toner set 
    quantity = quantity - %s
    where id = %s;
    """
    cnx.Params = ((cant + backup_quantity), toner_id)
    cnx.execute_non_query()
    return toner_inventory_id


@app.route('/add_assignment', methods=['POST'])
@login_required
def add_assignment():
    json_obj = json.loads(request.data.decode('utf-8'))
    with MSQLTrans() as cnx:
        cnx.Command = """\
        insert into toner_sent(agency_id, toner_user_id, post_date)
        values (%s, %s, UTC_TIMESTAMP());
        """
        cnx.Params = (json_obj['agency_id'], json_obj['toner_user_id'])
        toner_sent_id = cnx.insert()
        for row in json_obj['printers']:
            printer_id = row['printer_id'] if row['printer_id'] != 0 else None
            for toner in row['toners']:
                # comprobando inventario
                cnx.Command = """\
                select quantity from toner
                where id = %s;
                """
                cnx.Params = (toner['toner_id'],)
                quantity = cnx.execute_get_column('quantity')
                if quantity < toner['backup_quantity'] or quantity < 0:
                    return make_response(jsonify({"message": "little inventory"}), 500)
                # vincula el nuevo toner
                cnx.Command = """\
                insert detail_toner_sent (toner_sent_id, toner_id, printer_id, backup_quantity)
                values (%s, %s, %s, %s);
                """
                cnx.Params = (
                    toner_sent_id, toner['toner_id'], printer_id, toner['backup_quantity'])
                cnx.execute_non_query()
                # disminuye inventario
                __inventory_update(
                    cnx, json_obj['agency_id'], toner['toner_id'], toner['backup_quantity'])
                # marcando que existe un toner esperando
                if printer_id:
                    cnx.Command = """\
                    UPDATE toner_using set isWaitingForChange = 1
                    WHERE toner_using.printer_id = %s and toner_using.toner_id = %s;
                    """
                    cnx.Params = (row['printer_id'], toner['toner_id'])
                    cnx.execute_non_query()
    return make_response(jsonify({"message": "success"}), 201)


@app.route('/get_last_assignment', methods=['POST'])
@login_required
def get_last_assignment():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    print(json_obj)
    with mysql_con() as cnx:
        cnx.Command = """\
        select toner_sent.id, usr.name as 'user', agency.name as 'agency', post_date 
        from toner_sent
        inner join agency on toner_sent.agency_id = agency.id
        inner join toner_user_view usr on toner_sent.toner_user_id = usr.id
        where date(toner_sent.post_date) between %s and %s
        order by toner_sent.id desc
        """
        cnx.Params = (json_obj['start_date'], json_obj['end_date'])
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "number": row['id'],
                    "user": row['user'],
                    "agency": row['agency'],
                    "post_date": __datetime_format(row['post_date'])
                })
    return jsonify(result)


@app.route('/get_assignment_by_number', methods=['GET'])
@login_required
def get_assignment_by_number():
    number = int(request.args.get('assignment_number'))
    result = {
        "number": number,
        "agency": None,
        "user": None,
        "post_date": None,
        "printers": []
    }
    with mysql_con() as cnx:
        cnx.Command = """
        select toner_sent.id,
        toner_sent.post_date,
        usr.name as 'user',
        agency.name as 'agency'
        from toner_sent 
        inner join toner_user_view usr on toner_sent.toner_user_id = usr.id
        inner join agency on toner_sent.agency_id = agency.id
        where toner_sent.id = %s;
        """
        cnx.Params = (number,)
        with cnx.get_reader() as reader:
            for row in reader:
                result['agency'] = row['agency']
                result['user'] = row['user']
                result['post_date'] = __datetime_format(row['post_date'])
                cnx.Command = """
                select
                IFNULL(detail.printer_id, 0) as 'printer_id', 
                IFNULL(printer.name, 'Backup') as 'printer' ,
                toner.name as 'toner',
                detail.backup_quantity,
                IF(detail.toner_assignment_info_id is null, 1, 0)  as 'isWaitingForChange'
                from detail_toner_sent detail
                left join printer on detail.printer_id = printer.id
                inner join toner on detail.toner_id = toner.id
                where detail.toner_sent_id = %s
                order by detail.printer_id asc, detail.toner_id;
                """
                cnx.Params = (row['id'],)
                aux = []
                with cnx.get_reader() as reader2:
                    for detail in reader2:
                        aux.append({
                            "printer_id": detail['printer_id'],
                            "printer": detail['printer'],
                            "toner": detail['toner'],
                            "backup_quantity": detail['backup_quantity'],
                            "isWaitingForChange": detail['isWaitingForChange']
                        })
                for id, toners in groupby(aux, key=lambda x: x['printer_id']):
                    aux_printer = {
                        'printer': None,
                        'toners': []
                    }
                    for toner in toners:
                        aux_printer['printer'] = toner['printer']
                        aux_printer['toners'].append({
                            "toner": toner['toner'],
                            "backup_quantity": toner['backup_quantity'],
                            "isWaitingForChange": toner['isWaitingForChange']
                        })
                    result['printers'].append(aux_printer)
    return jsonify(result)


@app.route('/get_inventory_by_agency', methods=['POST'])
@login_required
def get_inventory_by_agency():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select toner.id, toner.name, toner_inventory.quantity, toner_inventory.residual_quantity,
        toner_inventory.backup_quantity
        from toner
        LEFT JOIN toner_inventory on toner.id = toner_inventory.toner_id
        and toner_inventory.agency_id = %s
        order by toner.id
        """
        cnx.Params = (json_obj['agency_id'], )
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "toner_id": row['id'],
                    "name": row['name'],
                    "quantity": row['quantity'] or 0,
                    "backup_quantity": row['backup_quantity'] or 0,
                    "residual_quantity": row['residual_quantity']
                })
    return jsonify(result)


@app.route('/get_all_inventory', methods=['GET'])
#@login_required
def get_all_inventory():
    result = []
    with mysql_con() as cnx:
        # inventario dealergeek
        cnx.Command = """
        select name, quantity from toner
        """
        dg = {
            'name': 'Dealergeek',
            'toners': []
        }
        with cnx.get_reader() as reader:
            for row in reader:
                dg['toners'].append({
                    "name": row['name'],
                    "quantity": row['quantity'] or 0,
                    "backup_quantity": 0
                })
        result.append(dg)
        # agencias
        cnx.Command = """
        select toner_inventory.agency_id,
        agency.name as 'agency',
        toner.name, toner_inventory.quantity,
        toner_inventory.backup_quantity
        from toner
        LEFT JOIN toner_inventory on toner.id = toner_inventory.toner_id
        LEFT JOIN agency on toner_inventory.agency_id = agency.id
        order by agency.id, toner.id
        """
        with cnx.get_reader() as reader:
            agency_id = 0
            agency = None
            for row in reader:
                if agency_id != row['agency_id']:
                    if agency:
                        result.append(agency)
                    agency = {
                        'name': row['agency'],
                        'toners': []
                    }
                    agency_id = row['agency_id']
                agency['toners'].append({
                    "name": row['name'],
                    "quantity": row['quantity'] or 0,
                    "backup_quantity": row['backup_quantity'] or 0
                })
            result.append(agency)
    return jsonify(result)


@app.route('/get_assignment_toner_no_match', methods=['GET'])
@login_required
def get_assignment_toner_no_match():
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        SELECT toner_using.*,
        printer.name as 'printer',
        agency.name as 'agency',
        toner.name as 'toner',
        info.serial_number as 'serial_number_installed',
        (select toner_info.serial_number from toner_info where toner_using.toner_info_assignment_id = toner_info.id) as 'serial_number_assignment'
        FROM toner_using
        INNER JOIN printer on toner_using.printer_id = printer.id
        INNER JOIN agency on printer.agency_id = agency.id
        INNER JOIN toner_info info on toner_using.toner_info_installed_id = info.id
        INNER JOIN toner on info.toner_id = toner.id
        where toner_using.toner_info_assignment_id is null or toner_using.toner_info_installed_id <> toner_using.toner_info_assignment_id
        ORDER BY agency.name ASC, printer.id ASC, toner.id ASC
        """
        cnx.Params = None
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "id": row['id'],
                    "agency": row['agency'],
                    "printer": row['printer'],
                    "toner": row['toner'],
                    "serial_number_installed": row['serial_number_installed'],
                    "serial_number_assignment": row['serial_number_assignment'],
                })
    return jsonify(result)


@app.route('/get_toner_no_assignment', methods=['GET'])
@login_required
def get_toner_no_assignment():
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select
        subnet.name as 'subnet',
        printer.name as 'printer',
        toner.name as 'toner',
        toner_sent.post_date
        from detail_toner_sent detail
        inner join toner_sent on detail.toner_sent_id = toner_sent.id
        inner join printer on detail.printer_id = printer.id
        inner join subnet on printer.subnet_id = subnet.id
        inner join toner on detail.toner_id = toner.id
        where detail.toner_assignment_info_id is null
        order by subnet.id asc, printer.id asc, toner.id asc;
        """
        cnx.Params = None
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "subnet": row['subnet'],
                    "printer": row['printer'],
                    "toner": row['toner'],
                    "post_date": __date_format(row['post_date'])
                })
    return jsonify(result)


@app.route('/toner_to_change', methods=['POST'])
@login_required
def toner_to_change():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        select
        subnet.name as 'subnet',
        printer.name as 'printer',
        toner.name as 'toner',
        toner_info.percent,
        toner_using.isWaitingForChange
        from toner_using
        left join toner_info on toner_using.toner_info_installed_id = toner_info.id
        inner join printer on toner_using.printer_id = printer.id
        inner join toner on toner_using.toner_id = toner.id
        inner join subnet on printer.subnet_id = subnet.id
        where (toner_using.toner_info_installed_id is null
        or toner_info.percent <= 15) and subnet.id = %s
        order by subnet.id, printer.id, toner.id;
        """
        cnx.Params = (json_obj['subnet_id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "subnet": row['subnet'],
                    "printer": row['printer'],
                    "toner": row['toner'],
                    "percent": row['percent'],
                    "isWaitingForChange": row['isWaitingForChange']
                })
    return jsonify(result)


@app.route('/get_inventory_residual_quantity', methods=['POST'])
@login_required
def get_inventory_residual_quantity():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """\
        SELECT toner_inventory.toner_id,
        toner.name as 'toner',
        toner_inventory.residual_quantity
        FROM toner_inventory
        INNER JOIN toner on toner_inventory.toner_id = toner.id
        WHERE toner_inventory.agency_id = %s and toner_inventory.residual_quantity > 0
        order by toner_inventory.toner_id
        """
        cnx.Params = (json_obj['agency_id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "toner_id": row['toner_id'],
                    "toner": row['toner'],
                    "residual_quantity": row['residual_quantity']
                })
    return jsonify(result)


@app.route('/add_residual_toner', methods=['POST'])
@login_required
def add_residual_toner():
    json_obj = json.loads(request.data.decode('utf-8'))
    with MSQLTrans() as cnx:
        cnx.Command = """\
        insert into residual_toner(toner_user_id, agency_id, residual_date, post_date)
        values (%s, %s, %s, UTC_TIMESTAMP());
        """
        cnx.Params = (json_obj['toner_user_id'],
                      json_obj['agency_id'], json_obj['residual_date'])
        residual_toner_id = cnx.insert()
        for toner in json_obj['toners']:
            cnx.Command = """\
            insert into detail_residual_toner (residual_toner_id, toner_id, quantity)
            values (%s, %s, %s);
            """
            cnx.Params = (residual_toner_id,
                          toner['toner_id'], toner['residual_quantity'])
            cnx.execute_non_query()
    return success_ok()


@app.route('/get_last_residual_toner', methods=['POST'])
@login_required
def get_last_residual_toner():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    print(json_obj)
    with mysql_con() as cnx:
        cnx.Command = """\
        select 
        residual_toner.id,
        agency.name as 'agency',
        residual_toner.residual_date,
        usr.name as 'user'
        from residual_toner
        inner join agency on residual_toner.agency_id = agency.id
        inner join toner_user_view usr on residual_toner.toner_user_id = usr.id
        where residual_toner.residual_date between %s and %s
        order by  residual_toner.id desc;
        """
        cnx.Params = (json_obj['start_date'], json_obj['end_date'])
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "number": row['id'],
                    "agency": row['agency'],
                    "residual_date": __date_format(row['residual_date']),
                    "user": row['user']
                })
    return jsonify(result)


@app.route('/get_residual_by_number', methods=['GET'])
@login_required
def get_residual_by_number():
    residual_number = int(request.args.get('residual_number'))
    result = {
        "id": 0,
        "number": residual_number,
        "agency": None,
        "user": None,
        "residual_date": None,
        "post_date": None,
        "toners": []
    }
    with mysql_con() as cnx:
        cnx.Command = """\
        select residual_toner.*,
        toner_user_view.name as 'user',
        agency.name as 'agency'
        from residual_toner
        INNER JOIN agency on residual_toner.agency_id = agency.id
        INNER JOIN toner_user_view on residual_toner.toner_user_id = toner_user_view.id
        where residual_toner.id = %s;
        """
        cnx.Params = (residual_number, )
        with cnx.get_reader() as reader:
            for row in reader:
                result['id'] = row['id']
                result['user'] = row['user']
                result['agency'] = row['agency']
                result['residual_date'] = __date_format(row['residual_date'])
                result['post_date'] = __datetime_format(row['post_date'])
        cnx.Command = """\
        select detail.quantity,
        toner.name as 'toner'
        from detail_residual_toner detail
        INNER JOIN toner on detail.toner_id = toner.id
        where detail.residual_toner_id = %s
        order by toner.id;
        """
        cnx.Params = (result['id'],)
        with cnx.get_reader() as reader:
            for row in reader:
                result['toners'].append({
                    "toner": row['toner'],
                    "quantity": row['quantity']
                })
    return jsonify(result)


@app.route('/get_report_printer', methods=['POST'])
@login_required
def get_report_printer():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = {
        'labels': [],
        'datasets': []
    }
    with mysql_con() as cnx:
        cnx.Command = """\
        select id, name, color_hex from toner
        """
        cnx.Params = None
        with cnx.get_reader() as toner_reader:
            count = 0
            for row_toner in toner_reader:
                count += 1
                dataset = {
                    "label": row_toner['name'],
                    "borderColor": row_toner['color_hex'],
                    'backgroundColor': row_toner['color_hex'],
                    "stack": 'stack ' + str(count),
                    "data": []
                }
                cnx.Command = """\
                select printer.id, printer.name from installed_toner
                inner join printer on installed_toner.printer_id = printer.id
                inner join toner_info on installed_toner.toner_info_id = toner_info.id
                where printer.subnet_id = %s and toner_info.toner_id = %s
                and date(installed_toner.instalation_date) BETWEEN %s and %s
                GROUP BY printer.id
                """
                cnx.Params = (json_obj['subnet_id'], row_toner['id'],
                              json_obj['start_date'], json_obj['end_date'])
                with cnx.get_reader() as reader:
                    for row in reader:
                        cnx.Command = """\
                        select toner.name, COUNT(installed_toner.id) as 'count' from installed_toner
                        INNER JOIN toner_info on installed_toner.toner_info_id = toner_info.id
                        INNER JOIN toner on toner_info.toner_id = toner.id
                        where installed_toner.printer_id = %s and toner.id = %s
                        GROUP BY toner.id
                        """
                        cnx.Params = (row['id'], row_toner['id'])
                        if not row['name'] in result['labels']:
                            result['labels'].append(row['name'])
                        with cnx.get_reader() as cont_reader:
                            for aux in cont_reader:
                                dataset['data'].append(aux['count'])
                result['datasets'].append(dataset)
    return jsonify(result)


@app.route('/get_report_printer_installed', methods=['POST'])
@login_required
def get_report_printer_installed():
    json_obj = json.loads(request.data.decode('utf-8'))
    result = []
    with mysql_con() as cnx:
        cnx.Command = """
        select
        subnet.name as 'subnet',
        printer.name as 'printer',
        toner.name as 'toner',
        toner_info.serial_number,
        installed_toner.instalation_date
        from installed_toner
        inner join printer on installed_toner.printer_id = printer.id
        inner join subnet on printer.subnet_id = subnet.id
        inner join toner_info on installed_toner.toner_info_id = toner_info.id
        inner join toner on toner_info.toner_id = toner.id
        where IF(%s > 0, printer.subnet_id = %s, 1)
        and IF(%s > 0, printer.id = %s, 1)
        and IF(%s > 0, toner.id = %s, 1)
        and date(installed_toner.instalation_date) between %s and %s
        order by installed_toner.instalation_date;
        """
        cnx.Params = (json_obj['subnet_id'], json_obj['subnet_id'],
                      json_obj['printer_id'], json_obj['printer_id'],
                      json_obj['toner_id'], json_obj['toner_id'],
                      json_obj['start_date'], json_obj['end_date'])
        with cnx.get_reader() as reader:
            for row in reader:
                result.append({
                    "subnet": row['subnet'],
                    "printer": row['printer'],
                    "toner": row['toner'],
                    "serial_number": row['serial_number'],
                    "instalation_date": __datetime_format(row['instalation_date'])
                })
    return jsonify(result)


@app.route('/send_report_printer_installed', methods=['POST'])
@login_required
def send_report_printer_installed():
    json_obj = json.loads(request.data.decode('utf-8'))
    aux = ""
    for row in json_obj['toners']:
        aux = aux + """
        <tr>
            <td scope="row">{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
            <td>{4}</td>
        </tr>
        """.format(
            row['subnet'],
            row['printer'],
            row['toner'],
            row['serial_number'],
            row['instalation_date']
        )
    style = """\
    <style>
        .subnet {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .subnet td, .subnet th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .subnet tr:nth-child(even){background-color: #f2f2f2;}

        .subnet tr:hover {background-color: #ddd;}

        .subnet th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4c61af;
            color: white;
        }
        </style>
    """
    doc_html = """ \
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        {0}
    </head>
    <body>
        <div>
            <h3>Toner report installed</h3>
            <div>
                <b>Dealership:</b> <small>{1}</small>
            </div>
            <div>
                <b>Printer:</b> <small>{2}</small>
            </div>
            <div>
                <b>Toner:</b> <small>{3}</small>
            </div>
            <div>
                <b>Start date:</b> <small>{4}</small>
            </div>
            <div>
                <b>End date:</b> <small>{5}</small>
            </div>
            <br />
            <br />
            <div class="table-responsive">
                <table class="subnet">
                    <thead>
                        <tr>
                            <th scope="col">Subnet</th>
                            <th scope="col">Printer</th>
                            <th scope="col">Toner</th>
                            <th scope="col">Serial number</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {6}
                    </tbody>
                </table>
            </div>
        </div>
    </body>
    </html>
    """.format(
        style,
        json_obj['subnet'],
        json_obj['printer'],
        json_obj['toner'],
        json_obj['start_date'],
        json_obj['end_date'],
        aux
    )
    nEmail = {
        'subject': 'Toner report installed',
        'ccs': json_obj['ccs'],
        'body': doc_html
    }
    send_email(nEmail)
    return success_ok()


def __date_format(date_value):
    """Da formato a la fecha"""
    return date_value.strftime('%Y-%m-%d') if date_value else None


def __datetime_format(date_value):
    """Da formato a la fecha"""
    return date_value.strftime('%Y-%m-%d %H:%M:%S') if date_value else None


if __name__ == '__main__':
    app.run(debug=DEBUG, host='0.0.0.0', port=5003)
