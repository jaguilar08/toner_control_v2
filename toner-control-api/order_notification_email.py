from const import DEBUG, CCS
from mySQLCon import mysql_con
from send_email import send_email, SUPPORT_EMAIL

def __datetime_format(date_value):
    """Da formato a la fecha"""
    return date_value.strftime('%Y-%m-%d') if date_value else None

orders = []
# obtiene la informacion desde la bd
with mysql_con() as cnx:
    # obtiene las ordenes a enviar
    cnx.Command = """
    select id, number, post_date from toner_order
    where timestampdiff(DAY, cast(toner_order.post_date as date), UTC_DATE()) >= 7 and pending_verified = 1
    order by post_date asc; 
    """
    cnx.Params = None
    with cnx.get_reader() as reader:
        for row in reader:
            # detalle de la orden
            cnx.Command = """
            select toner.name, detail.quantity, detail.pending_quantity,
            (detail.quantity - detail.pending_quantity) as 'received_quantity'
            from detail_toner_order detail
            inner join toner on detail.toner_id = toner.id
            where detail.toner_order_id = %s and detail.pending_quantity > 0
            order by detail.toner_id asc;
            """
            cnx.Params = (row['id'],)
            order = {
                'number': row['number'],
                'post_date': __datetime_format(row['post_date']),
                'details': []
            }
            with cnx.get_reader() as reader2:
                for row2 in reader2:
                    order['details'].append({
                        'toner': row2['name'],
                        'quantity_ordered': row2['quantity'],
                        'pending_quantity': row2['pending_quantity'],
                        'received_quantity': row2['received_quantity']
                    })
            orders.append(order)

# creando cuerpo del email
if (len(orders) > 0):  # si existen ordenes
    aux = ""
    lines = []
    for row in orders:
        aux2 = ""
        for detail in row['details']:
            aux2 = aux2 + """
            <tr>
                <td scope="row">{0}</td>
                <td scope="row">{1}</td>
                <td scope="row">{2}</td>
                <td scope="row">{3}</td>
            </tr>
            """.format(
                detail['toner'],
                detail['quantity_ordered'],
                detail['received_quantity'],
                detail['pending_quantity']
            )
        aux = aux + """
        <div>
            <div>
                <b>Order number:</b> <small>{0}</small>
            </div>
            <div>
                <b>Post date:</b> <small>{1}</small>
            </div>
             <div class="table-responsive">
                <table class="subnet">
                    <thead>
                        <tr>
                            <th scope="col">Description</th>
                            <th scope="col">Qty ordered</th>
                            <th scope="col">Qty received</th>
                            <th scope="col">Qty pending</th>
                        </tr>
                    </thead>
                    <tbody>
                        {2}
                    </tbody>
                </table>
            </div>
        </div>
        <br />
        <br />
        """.format(
            row['number'],
            row['post_date'],
            aux2
        )
    style = """\
    <style>
        .subnet {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .subnet td, .subnet th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .subnet tr:nth-child(even){background-color: #f2f2f2;}

        .subnet tr:hover {background-color: #ddd;}

        .subnet th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4c61af;
            color: white;
        }
        </style>
    """
    doc_html = """ \
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        {0}
    </head>
    <body>
        <div>
            <h3>Pending orders</h3>
            {1}
        </div>
    </body>
    </html>
    """.format(style, aux)
    nEmail = {
        'subject': 'Report pending orders to be verified',
        'from': '"Toner control system"',
        'ccs': CCS,
        'body': doc_html
    }
    send_email(nEmail)
