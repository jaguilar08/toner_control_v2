
# identificador de la aplicacion
application_id = 3

# indica que está modo debug
DEBUG = False

# copia de reporte cuando
# la orden queda pendiente
CCS = ('jaguilar@dealergeek.com', 'flopez@dealergeek.com',
       'bgriffith@dealergeek.com', 'wgriffith@dealergeek.com')


# si modo debug está activado solo envia correo a jeff
if DEBUG:
    CCS = ('jaguilar@dealergeek.com',)