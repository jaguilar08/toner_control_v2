import http.client
from const import application_id
from flask import request, json


class DGAPI:
    server = 'localhost:8015'  # "173.235.74.153:8015"

    @staticmethod
    def get_security_token():
        """Obtiene el token enviado como cabecera"""
        if request and 'security-token' in request.headers:
            return request.headers.get('security-token')
        return ''

    def send_request(self, type_method, endpoint, json_param) -> json:
        conn = http.client.HTTPConnection(self.server)
        headers = {
            'content-type': "application/json",
            'security-token': self.get_security_token(),  # "ef350d20-dcd9-4d53-a2d8-b1a8131b5dff",
            'application-id': application_id,
            'cache-control': "no-cache"
        }
        conn.request(type_method, "/dgapi/" + endpoint, json.dumps(json_param), headers=headers)
        with conn.getresponse() as response:
            return {
                "status": response.status,
                "response": response.read().decode("utf-8")
            }

    def post(self, method, params) -> json:
        return self.send_request('POST', method, params)

    def get(self, method) -> json:
        return self.send_request('GET', method, None)

# dg = DGAPI()
# print(dg.get("get_all_carrier"))
