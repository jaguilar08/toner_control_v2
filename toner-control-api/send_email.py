import imaplib
import email
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

SUPPORT_EMAIL = "jaguilar@dealergeek.com"
PASSWORD = "aguilar08"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT = 587


def send_email(nEmail):
    """Envia correo electronico"""
    msg = MIMEMultipart('alternative')
    msg.set_charset("utf-8")
    msg['Subject'] = nEmail['subject']
    msg['From'] = nEmail['from'] if 'from' in nEmail else SUPPORT_EMAIL
    msg['To'] = ', '.join(nEmail['ccs'])
    text = "Toner control"
    #msg.add_header('ticket-id', '<{0}>'.format(ticket['id']))
    part1 = MIMEText(text, 'plain')
    part1.set_charset("utf-8")
    part2 = MIMEText(nEmail['body'], 'html')
    part2.set_charset("utf-8")
    # Agrega las partes al contenido del mensaje
    msg.attach(part1)
    msg.attach(part2)
    # attach image to message body
    # msg.attach(MIMEImage(file("google.jpg").read()))
    # Envia mensaje via SMTP
    mail = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    mail.ehlo()
    mail.starttls()
    mail.login(SUPPORT_EMAIL, PASSWORD)
    mail.sendmail(SUPPORT_EMAIL, nEmail['ccs'], msg.as_string())
    mail.quit()
    print('enviado')
