import time
from itertools import chain
import email
import imaplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from lxml.html import fromstring
import re
import os

TIME_SUSPEND = 3  # tiempo en segundo para suspender la ejecución
imap_ssl_host = 'imap.gmail.com'  # imap.mail.yahoo.com
imap_ssl_port = 993
username = 'jaguilar@dealergeek.com'
password = 'aguilar08'


def get_body_email(msg):
    type = msg.get_content_maintype()
    if type == 'multipart':
        for part in msg.get_payload():
            if part.get_content_type() == 'text/plain':
                return part.get_payload(decode=True).decode('UTF-8')

while 1:
    server = imaplib.IMAP4_SSL(imap_ssl_host, imap_ssl_port)
    server.login(username, password)
    try:
        server.select('INBOX')
        tp, data =  server.search(None, '(UNSEEN Subject "command")')#server.search(None, 'UNSEEN')
        for email_id in data[0].split():
            status, data = server.fetch(email_id, '(RFC822)')
            msg = email.message_from_bytes(data[0][1])
            command = get_body_email(msg) # obtiene el texto
            print(command)
            if command == 'full_tonner_report':
                os.system("python /home/jaguilar/tonner_control/tonner.service.py full_tonner_report")
            elif command == 'low_tonner_report':
                os.system("python /home/jaguilar/tonner_control/tonner.service.py low_tonner_report")
    except Exception as ex:
        raise ex
    finally:
        if server.state == 'SELECTED':
            server.close()
        server.logout()
        time.sleep(TIME_SUSPEND)
