create database dbtonnercontrol

use dbtonnercontrol

create table toner_user
(
id int AUTO_INCREMENT primary key not null,
user_application_id int not null,
constraint fk_toner_user_user_application_id foreign key (user_application_id) references dbdealergeek.user_application(id)
);

/*agencias*/
create table agency
(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
name varchar(30) UNIQUE NOT NULL,
subnet varchar(15) UNIQUE NOT NULL
);

create table subnet
(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
agency_id int NOT NULL,
name varchar(30) UNIQUE NOT NULL,
ip_adress varchar(15) UNIQUE NOT NULL,
CONSTRAINT fk_subnet_agency_id FOREIGN KEY (agency_id) REFERENCES agency(id)
);

/*toner*/
create table toner(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
name varchar(30) UNIQUE NOT NULL,
quantity int default 0 NOT NULL,
color_hex varchar(10) DEFAULT '#566374' NOT NULL,
font_color varchar(10) DEFAULT '#FFFFFF' NOT NULL
);

create table toner_inventory(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
agency_id int NOT NULL,
toner_id int NOT NULL,
quantity int DEFAULT 0 NOT NULL,
backup_quantity int DEFAULT 0 NOT NULL COMMENT 'Cantidad asignada para backup',
residual_quantity int DEFAULT 0 NOT NULL COMMENT 'Cantidad de toner vacios que debe regresar',
CONSTRAINT fk_toner_inventory_agency_id FOREIGN KEY (agency_id) REFERENCES agency(id),
CONSTRAINT fk_toner_inventory_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id),
UNIQUE(agency_id, toner_id)
);


/*Impresoras*/
create table printer(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
name varchar(60) not null,
subnet_id int NOT NULL,
ip_adress varchar(15) UNIQUE NOT NULL,
CONSTRAINT fk_printer_subnet_id FOREIGN KEY (subnet_id) REFERENCES subnet(id)
);

/*Informacion del toner*/
create TABLE toner_info(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_id int NOT NULL,
serial_number varchar(50) UNIQUE NOT NULL,
percent int DEFAULT 100 NOT NULL COMMENT 'Porcentaje del toner',
page_printed int DEFAULT 0 NOT NULL COMMENT 'Paginas impresas',
last_update datetime NOT NULL COMMENT 'ultima actualizacion',
CONSTRAINT fk_toner_info_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id)
);

/*Toner que deben volver a dealergeek*/
create table toner_inventory_residual(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_inventory_id int NOT NULL,
printer_id int NOT NULL,
toner_info_id int NOT NULL,
date_post datetime NOT NULL,
CONSTRAINT fk_toner_inventory_residual_toner_inventory_id FOREIGN KEY (toner_inventory_id) REFERENCES toner_inventory(id),
CONSTRAINT fk_toner_inventory_residual_printer_id FOREIGN KEY (printer_id) REFERENCES printer(id),
CONSTRAINT fk_toner_inventory_residual_toner_info_id FOREIGN KEY (toner_info_id) REFERENCES toner_info(id)
);




/*Orden de toner, alimentación de inventario*/
create table toner_order(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_user_id int NOT NULL,
number varchar(30) UNIQUE NOT NULL,
post_date datetime NOT NULL,
order_date datetime NOT NULL,
CONSTRAINT fk_toner_order_toner_user_id FOREIGN KEY (toner_user_id) REFERENCES toner_user(id)
);

create table detail_toner_order(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_order_id int NOT NULL,
toner_id int NOT NULL,
quantity int default 0 not null,
CONSTRAINT fk_detail_toner_order_toner_order_id FOREIGN KEY (toner_order_id) REFERENCES toner_order(id),
CONSTRAINT fk_detail_toner_order_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id)
);

alter table detail_toner_order
add column pending_quantity int DEFAULT 0 NOT NULL;

alter table toner_order
add column pending_verified tinyint(1) DEFAULT 1 NOT NULL COMMENT 'Orden pendiente por verificar';


/*Verificacion de orden*/

create table toner_order_verified(
    id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
    toner_user_id int NOT NULL,
    toner_order_id int NOT NULL,
    post_date datetime NOT NULL,
    CONSTRAINT fk_toner_order_verified_toner_user_id FOREIGN KEY (toner_user_id) REFERENCES toner_user(id),
    CONSTRAINT fk_toner_order_verified_toner_order_id FOREIGN KEY (toner_order_id) REFERENCES toner_order(id)
);


create table detail_toner_order_verified(
    id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
    toner_order_verified_id int NOT NULL,
    detail_toner_order_id int NOT NULL,
    before_quantity int DEFAULT 0 NOT NULL COMMENT 'Valor anterior',
    received_quantity int DEFAULT 0 NOT NULL COMMENT 'Valor recibido',
    after_quantity int as (before_quantity - received_quantity) STORED COMMENT 'Valor despues',
    CONSTRAINT fk_detail_toner_order_verified_toner_order_verified_id FOREIGN KEY (toner_order_verified_id) REFERENCES toner_order_verified(id),
    CONSTRAINT fk_detail_toner_order_verified_detail_toner_order_id FOREIGN KEY (detail_toner_order_id) REFERENCES detail_toner_order(id)
);

/*create table toner_allocation(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_user_id int NOT NULL,
printer_id int NOT NULL,
post_date datetime NOT NULL,
CONSTRAINT fk_toner_allocation_toner_user_id FOREIGN KEY (toner_user_id) REFERENCES toner_user(id),
CONSTRAINT fk_toner_allocation_printer_id FOREIGN KEY (printer_id) REFERENCES printer(id)
);


create table detail_toner_allocation(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_allocation_id int NOT NULL,
toner_id int NOT NULL,
toner_assignment_info_id int NULL COMMENT 'Toner asignado',
CONSTRAINT fk_detail_toner_allocation_toner_allocation_id FOREIGN KEY (toner_allocation_id) REFERENCES toner_allocation(id),
CONSTRAINT fk_detail_toner_allocation_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id),
CONSTRAINT fk_detail_toner_allocation_toner_assignment_info_id FOREIGN KEY (toner_assignment_info_id) REFERENCES toner_info(id)
);*/

create table toner_sent(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_user_id int NOT NULL,
agency_id int NOT NULL,
post_date datetime NOT NULL,
CONSTRAINT fk_toner_sent_toner_user_id FOREIGN KEY (toner_user_id) REFERENCES toner_user(id),
CONSTRAINT fk_toner_sent_agency_id FOREIGN KEY (agency_id) REFERENCES agency(id)
);

create table detail_toner_sent(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_sent_id int NOT NULL,
toner_id int NOT NULL,
printer_id int COMMENT 'Si es null es backup',
backup_quantity int DEFAULT 0 NOT NULL,
toner_assignment_info_id int NULL COMMENT 'Toner asignado',
CONSTRAINT fk_detail_toner_sent_toner_sent_id FOREIGN KEY (toner_sent_id) REFERENCES toner_sent(id),
CONSTRAINT fk_detail_toner_sent_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id),
CONSTRAINT fk_detail_toner_sent_printer_id FOREIGN KEY (printer_id) REFERENCES printer(id),
CONSTRAINT fk_detail_toner_sent_toner_assignment_info_id FOREIGN KEY (toner_assignment_info_id) REFERENCES toner_info(id)
);

/*Instalacion del toner*/
create table installed_toner(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
printer_id int NOT NULL,
toner_info_id int NOT NULL,
instalation_date datetime NOT NULL,
CONSTRAINT fk_installed_toner_printer_id FOREIGN KEY (printer_id) REFERENCES printer(id),
CONSTRAINT fk_installed_toner_toner_info_id FOREIGN KEY (toner_info_id) REFERENCES toner_info(id)
);


/*Toner que estan en uso en la impresora*/
create table toner_using(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
printer_id int NOT NULL,
toner_id int NOT NULL,
toner_info_installed_id int COMMENT 'toner instalado en la impresora',
isWaitingForChange tinyint(1) DEFAULT 0 NOT NULL COMMENT 'valor que indica que tiene un toner asignado para ser cambiado',
CONSTRAINT fk_toner_using_printer_id FOREIGN KEY (printer_id) REFERENCES printer(id),
CONSTRAINT fk_toner_using_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id),
CONSTRAINT fk_toner_using_toner_info_installed_id FOREIGN KEY (toner_info_installed_id) REFERENCES toner_info(id),
UNIQUE (printer_id, toner_id)
);

/*Cantidad del toner que deben volver a dealergeek*/
create table residual_toner(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
toner_user_id int NOT NULL,
agency_id int NOT NULL,
residual_date date NOT NULL,
post_date datetime NOT NULL,
CONSTRAINT fk_residual_toner_toner_user_id FOREIGN KEY (toner_user_id) REFERENCES toner_user(id),
CONSTRAINT fk_residual_toner_agency_id FOREIGN KEY (agency_id) REFERENCES agency(id)
);

/*Toner que deben volver a dealergeek*/
create table detail_residual_toner(
id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
residual_toner_id int NOT NULL,
toner_id int NOT NULL,
quantity int DEFAULT 1 NOT NULL,
CONSTRAINT fk_detail_residual_toner_residual_toner_id FOREIGN KEY (residual_toner_id) REFERENCES residual_toner(id),
CONSTRAINT fk_detail_residual_toner_toner_id FOREIGN KEY (toner_id) REFERENCES toner(id),
UNIQUE(residual_toner_id, toner_id)
);

alter table printer
ADD COLUMN active tinyint(1) DEFAULT 1 NOT NULL COMMENT 'Impresora activa';



create table printer_model(
    id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
    model varchar(15) UNIQUE NOT NULL
);

ALTER TABLE printer
ADD COLUMN printer_model_id int NOT NULL,
ADD CONSTRAINT fk_printer_printer_model_id FOREIGN KEY (printer_model_id) REFERENCES printer_model(id),
ADD COLUMN last_update datetime DEFAULT NULL;

/*Procedure*/

/*DELIMITER $$
CREATE PROCEDURE proc_installed_toner(IN toner_id INT, IN agency_id INT)
    NO SQL
    COMMENT 'Afecta inventario del dealership al instalar toner'
UPDATE toner_inventory set quantity = quantity - 1
WHERE toner_inventory.toner_id = toner_id and toner_inventory.agency_id = agency_id
$$
DELIMITER ;*/

DELIMITER $$
CREATE PROCEDURE proc_installed_toner(toner_id INT, agency_id INT)
    NO SQL
    COMMENT 'Afecta inventario del dealership al instalar toner'
BEGIN
    SELECT id, quantity INTO @id, @quantity 
    from toner_inventory 
    WHERE toner_inventory.agency_id = agency_id 
    and toner_inventory.toner_id = toner_id;
    IF @quantity > 0 THEN
        UPDATE toner_inventory set quantity = quantity - 1
        WHERE toner_inventory.id = @id;
	ELSE
        UPDATE toner_inventory set backup_quantity = backup_quantity - 1
        WHERE toner_inventory.id = @id;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE proc_update_waiting_change()
    NO SQL
    COMMENT 'Actualiza todos los toner que esperan a ser cambiados'
UPDATE toner_using
LEFT JOIN (select 1 as 'aux', printer_id, toner_id from detail_toner_sent detail
          INNER JOIN toner_sent on detail.toner_sent_id = toner_sent.id) t2
          ON toner_using.printer_id = t2.printer_id AND toner_using.toner_id = t2.toner_id
set toner_using.isWaitingForChange = IFNULL(t2.aux, 0);
$$
DELIMITER ;



DELIMITER $$
CREATE PROCEDURE proc_update_pending_order(toner_order_id int)
    NO SQL
    COMMENT 'Actualiza el valor que indica si la orden está pendiente'
update toner_order SET
pending_verified = EXISTS(select id from detail_toner_order detail where detail.toner_order_id = toner_order_id AND pending_quantity > 0)
where id = toner_order_id$$
DELIMITER ;



/*Functions*/


/*Verifica el serial*/
DELIMITER $$
CREATE FUNCTION existToner(
    serial_number VARCHAR(50)
) 
RETURNS tinyint(4)
NO SQL
COMMENT 'Verifica si ya existe un toner con el serial que se especifica'
RETURN IFNULL((select 1 from toner_info where toner_info.serial_number = serial_number), 0)
$$
DELIMITER ;



/*Trigger*/

/*Alimenta el registro del toner
CREATE TRIGGER tr_add_order AFTER INSERT ON detail_toner_order
FOR EACH ROW UPDATE toner set quantity = quantity + new.quantity 
WHERE id = new.toner_id*/

/*Alimenta el inventario dealergeek*/
CREATE TRIGGER `tr_add_to_dg_inventory` AFTER INSERT ON `detail_toner_order_verified`
 FOR EACH ROW UPDATE toner
set quantity = quantity + new.received_quantity 
WHERE id = (select toner_id from detail_toner_order detail
            where detail.id = new.detail_toner_order_id)

/*Alimenta el inventario por dealership*/
/*DELIMITER ;;
CREATE TRIGGER tr_toner_allocation 
AFTER INSERT ON detail_toner_allocation
FOR EACH ROW
BEGIN
   /*Quita de inventario de dealergeek*/
   /*UPDATE toner set quantity = quantity - 1 WHERE id = new.toner_id;*/
   /*Inventario por dealergeek*/
   /*INSERT INTO toner_inventory (agency_id, toner_id, quantity)
   SELECT printer.agency_id, new.toner_id, 1  from toner_allocation
   INNER JOIN printer on toner_allocation.printer_id = printer.id
   WHERE toner_allocation.id = new.toner_allocation_id
   ON DUPLICATE KEY UPDATE 
      quantity = quantity + 1;*/
/*END ;;*/



DELIMITER ;;
CREATE TRIGGER tr_add_installed_toner
AFTER INSERT ON installed_toner
FOR EACH ROW
BEGIN
    SELECT agency_id INTO @agency_id FROM printer WHERE printer.id = new.printer_id;
    SELECT toner_id INTO @toner_id FROM toner_info WHERE toner_info.id = new.toner_info_id;
    UPDATE toner_inventory set quantity = quantity - 1
    WHERE toner_id = @toner_id and agency_id = @agency_id;
END ;;


/*Al agregar toner */
CREATE TRIGGER tr_add_toner AFTER INSERT ON toner
 FOR EACH ROW insert into toner_using (printer_id, toner_id)
select printer.id, new.id from printer

/*Al agregar impresoras */
CREATE TRIGGER tr_add_printer AFTER INSERT ON printer
 FOR EACH ROW insert into toner_using (printer_id, toner_id)
select new.id, toner.id from toner


CREATE TRIGGER tr_add_detail_residual_toner AFTER INSERT ON detail_residual_toner
 FOR EACH ROW UPDATE toner_inventory set 
 residual_quantity = residual_quantity - IF(residual_quantity < new.quantity, residual_quantity, new.quantity)
where toner_inventory.toner_id = new.toner_id and toner_inventory.agency_id = (select agency_id 
                                from residual_toner 
                                where new.residual_toner_id = residual_toner.id)

/*Actualiza la fecha de asignacion del toner*/
/*DELIMITER ;;
CREATE TRIGGER tr_update_detail_toner_allocation 
BEFORE UPDATE ON detail_toner_allocation
FOR EACH ROW
BEGIN
    IF new.replaced = 1 THEN
        SET NEW.date_replaced = UTC_TIMESTAMP();
    ELSE
        SET NEW.date_replaced = NULL;
    END IF;
    UPDATE toner set quantity = quantity - 1; 
END ;;*/


/*Vistas*/

create view toner_user_view
as
select toner_user.id, duser.name, duser.email  from toner_user
inner join dbdealergeek.user_application as userpp on userpp.id = toner_user.user_application_id
inner join dbdealergeek.duser as duser on userpp.duser_id = duser.id




/*Insert DEFAULT*/

/*inserta usuario*/
INSERT INTO toner_user(user_application_id) VALUES 
(22), (23), (24), (26);

/*Dealership*/
INSERT INTO agency (id, name, subnet) VALUES
(1, 'Ford', '10.1.2'),
(2, 'Toyota', '10.6.2');

INSERT INTO subnet(id, agency_id, name, ip_adress) VALUES
(1, 1, 'Ford', '10.1.2'),
(2, 1, 'Auto credit', '10.4.2'),
(3, 1, 'Accounting', '10.2.1'),
(4, 1, 'Nissan', '10.3.2'),
(5, 2,'Toyota', '10.6.2');

/*Toners*/
INSERT INTO toner (id, name, quantity, color_hex, font_color) VALUES
(1, 'CE400X, 507X Black', 0, '#000000', '#FFFFFF'),
(2, 'CE402A, 507A Yellow', 0, '#FFFF00', '#000000'),
(3, 'CE403A, 507A Magenta', 0, '#FF00FF', '#FFFFFF'),
(4, 'CE401A, 507A Cyan', 0, '#00FFFF', '#000000');