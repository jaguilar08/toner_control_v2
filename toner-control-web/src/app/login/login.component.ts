import { Component, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import { AppCurrentService } from '../app.current.service'
import { TonerService } from '../toner.service'
import { Md5 } from "md5-typescript";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  remember = false;
  username = '';
  password = '';

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private appCurrent: AppCurrentService,
    private tonerServ: TonerService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.remember = this.appCurrent.isRemember();
    if (this.appCurrent.isLogin()) {
      this.router.navigate(['home']);
    }
  }

  valid_user() {
    if (this.username == '') {
      this.toastr.warning('Empty', 'Empty username');
      return;
    }
    if (this.password == '') {
      this.toastr.warning('Empty', 'Empty password');
      return;
    }
    //this.spinner.show();
    this.tonerServ.authentication(this.username, Md5.init(this.password))
    .subscribe(response => {
      //console.log(response);
      //this.spinner.hide();
      this.appCurrent.setCredential(response, this.remember);
      this.router.navigate(['home']);
    }, error => {
      //console.log(error);
      //this.spinner.hide();
      this.toastr.error('unauthorized', 'Credentials are not valid');
    });
  }
}