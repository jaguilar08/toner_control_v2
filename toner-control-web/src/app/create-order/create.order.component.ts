import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { TonerService } from '../toner.service'
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import { MatDialog } from '@angular/material';
import * as moment from 'moment';

@Component({
    selector: 'app-create-order',
    templateUrl: './create.order.component.html',
    styleUrls: ['./create.order.component.css']
})
export class CreateOrderComponent {
    @Output() emitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() changeTitleEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output() loadData: EventEmitter<boolean> = new EventEmitter<boolean>();
    dealershipControl = new FormControl('', [Validators.required]);
    tonerControl = new FormControl('', [Validators.required]);
    title = 'New order';
    order_number = null;
    order_date = '';
    aDetail = [];
    isLoad = false;

    constructor(private tService: TonerService,
        private toastr: ToastrService,
        private route: ActivatedRoute,
        private router: Router,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.order_date = moment().format('YYYY-MM-DD');
        this.isLoad = true;
        this.tService.getToner().subscribe(data => {
            let detail = [];
            data.forEach(function (item) {
                item.quantity = 0;
                detail.push(item);
                //this.loadData.emit(false); // indica que termino de cargar
            });
            this.aDetail = detail;
            this.isLoad = false;
        });
    }

    // limpia los datos para cargar los nuevos
    clear_data() {
        this.aDetail.forEach(item => {
            item.quantity = 0;
        });
        this.order_number = null;
    }

    // registra nueva orden
    addOrder() {
        var detail = [];
        this.aDetail.forEach(function (item) {
            if (item.quantity > 0) {
                detail.push(item);
            }
        });
        if (this.order_number == null) {
            this.toastr.warning('Error', 'Specify the order number');
            return;
        }
        if (this.order_date == '') {
            this.toastr.warning('Error', 'Specify the date of the order');
            return;
        }
        if (detail.length == 0) {
            this.toastr.warning('Warning', 'Empty quantities');
            return;
        }
        var args = {
            "number": this.order_number,
            "order_date": this.order_date,
            "toners": detail
        }
        this.loadData.emit(true); // indica que termino de cargar
        this.tService.addOrder(args).subscribe(data => {
            this.toastr.success('Success', 'Saved order');
            this.emitEvent.emit(true);
            this.clear_data();
            this.loadData.emit(false); // indica que termino de cargar
        }, error => {
            this.toastr.error('Error', 'Error saving the order');
            this.loadData.emit(false); // indica que termino de cargar
        });
    }
}