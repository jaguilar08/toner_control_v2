import { Component, ViewChild } from '@angular/core';
import { HeaderComponent } from '../header/header.component'
import { CreateResidualTonerComponent } from './create-residual/create.residual.component'
import { TonerService } from '../toner.service'
import { ToastrService } from 'ngx-toastr';
import { ViewResidualTonerComponent } from './view-residual/view.residual.component'
import { ResidualInventoryTonerComponent } from './residual-inventory/residual.inventory.component'
import * as moment from 'moment';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-residual',
    templateUrl: './residual.component.html',
    styleUrls: ['./residual.component.css']
})
export class ResidualTonerComponent {
    @ViewChild('create') create: CreateResidualTonerComponent;
    @ViewChild('header') header: HeaderComponent;
    @ViewChild('residualView') residualView: ViewResidualTonerComponent;
    @ViewChild('inventory') inventory: ResidualInventoryTonerComponent;

    aResidualToners = [];
    isLoad = false;
    startDate = null;
    endDate = null;
    mode_view = false;
    order_number = null;

    constructor(
        private tService: TonerService,
        private toastr: ToastrService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    ngOnInit() {
        this.endDate = moment().format('YYYY-MM-DD');
        this.startDate = moment().add(-20, 'day').format('YYYY-MM-DD');
        this.route.params.subscribe(params => {
            let number = +params['id'];
            if (number) {
                this.mode_view = true;
                this.order_number = number;
                this.load_order(number);
            }
            else {
                this.mode_view = false;
            }
        });
        //crea
        this.create.showLoad.subscribe(s => {
            if (s) {
                this.header.show_load();
            }
            else {
                this.header.hide_load();
            }
        });
        this.create.create.subscribe(c => {
            this.load_data();
            this.inventory.load_data();
        });
        this.load_data();
    }

    // carga la orden
    load_order(order_number) {
        this.residualView.load_order(order_number);
    }

    // cierra la orden
    closeView() {
        this.router.navigate(['/residual']);
    }

    // carga los datos en lista
    load_data() {
        if (this.startDate == '') {
            this.toastr.error('Start date', 'Start date not selected');
            return;
        }
        if (this.endDate == '') {
            this.toastr.error('End date', 'End date not selected');
            return;
        }
        let args = {
            'start_date': moment(this.startDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
            'end_date': moment(this.endDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD')
        };
        this.isLoad = true;
        this.tService.getLastResidualToner(args).subscribe(data => {
            this.aResidualToners = data;
            this.isLoad = false;
        });
    }
}