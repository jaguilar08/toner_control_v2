import { Component } from '@angular/core';
import { TonerService } from '../../toner.service'
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-residual-inventory',
    templateUrl: './residual.inventory.component.html',
    styleUrls: ['./residual.inventory.component.css']
})
export class ResidualInventoryTonerComponent {
    dealershipControl = new FormControl('', [Validators.required]);
    aDealerships = [];
    aToners = [];
    selectedDealership = null;
    isLoad = false;

    constructor(
        private tService: TonerService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.isLoad = true;
        this.tService.getAgency().subscribe(data => {
            this.aDealerships = data;
            this.isLoad = false;
            if (this.aDealerships && this.aDealerships.length > 0) {
                this.selectedDealership = this.aDealerships[0].id;
                this.onSelectDealershipChange();
            }
        });
    }

    onSelectDealershipChange() {
        this.load_data();
    }

    load_data() {
        this.isLoad = true;
        this.tService.getInventoryByAgency(this.selectedDealership).subscribe(data => {
            this.aToners = data;
            this.isLoad = false;
        });
    }
}