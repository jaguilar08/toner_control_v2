import { Component, Output, EventEmitter } from '@angular/core';
import { TonerService } from '../../toner.service'
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Component({
    selector: 'app-create-residual',
    templateUrl: './create.residual.component.html',
    styleUrls: ['./create.residual.component.css']
})
export class CreateResidualTonerComponent {
    @Output() create: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() showLoad: EventEmitter<boolean> = new EventEmitter<boolean>();
    dealershipControl = new FormControl('', [Validators.required]);
    aDealerships = [];
    aDetail = [];
    selectedDealership = null;
    residual_date = '';
    isLoad = false;

    constructor(
        private tService: TonerService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.residual_date = moment().format('YYYY-MM-DD');
        this.tService.getAgency().subscribe(data => {
            this.aDealerships = data;
        });
    }

    onSelectDealershipChange() {
        this.isLoad = true;
        this.aDetail = [];
        this.tService.getInventoryByAgency(this.selectedDealership).subscribe(data => {
            let detail = [];
            data.forEach(function (item) {
                detail.push({
                    "toner_id": item.toner_id,
                    "toner": item.name,
                    "max_residual_quantity": item.residual_quantity,
                    "residual_quantity": 0
                });
            });
            this.aDetail = detail;
            this.isLoad = false;
        });
    }


    addOrder() {
        var detail = [];
        this.aDetail.forEach(function (item) {
            if (item.residual_quantity > 0) {
                detail.push(item);
            }
        });
        if (this.selectedDealership == null) {
            this.toastr.warning('Error', 'Specify the dealership');
            return;
        }
        if (this.residual_date == '') {
            this.toastr.warning('Error', 'Specify the date of the order');
            return;
        }
        if (detail.length == 0) {
            this.toastr.warning('Warning', 'Empty quantities');
            return;
        }
        var args = {
            "agency_id": this.selectedDealership,
            "residual_date": this.residual_date,
            "toners": detail
        }
        this.showLoad.emit(true);
        this.tService.addResidualToner(args).subscribe(data => {
            this.selectedDealership = null;
            this.aDetail = [];
            this.showLoad.emit(false);
            this.toastr.success('Success', 'Saved');
            this.create.emit(true);
        }, err => {
            this.showLoad.emit(false);
            this.toastr.error('Error', 'Error saving the residual toner');
        });
    }
}