import { Component } from '@angular/core';
import { TonerService } from '../../toner.service'
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Component({
    selector: 'app-view-residual',
    templateUrl: './view.residual.component.html',
    styleUrls: ['./view.residual.component.css']
})
export class ViewResidualTonerComponent {

    isLoad = false;
    Order = {};

    constructor(
        private tService: TonerService
    ) { }

    load_order(order_number) {
        this.Order = {
            "toners": []
        };
        this.isLoad = true;
        this.tService.getResidualByNumber(order_number).subscribe(data => {
            this.Order = data;
            this.isLoad = false;
        });
    }
}