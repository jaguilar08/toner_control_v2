import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'readableDate'})
export class ReadableDatePipe implements PipeTransform {
  transform(value: string): string {
    let _date = moment.utc(value,"YYYY-MM-DD HH:mm:ss").toDate();
    return moment(_date).local().format("MMMM DD YYYY, h:mm a");
  }
}

@Pipe({name: 'fmtDate'})
export class fmtDatePipe implements PipeTransform {
  transform(value: string): string {
    let _date = moment.utc(value,"YYYY-MM-DD HH:mm:ss").toDate();
    return moment(_date).local().format("YYYY-MM-DD HH:mm:ss");
  }
}