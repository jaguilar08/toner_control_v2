import { Injectable } from '@angular/core';
import {CanActivate} from "@angular/router";
import { AppCurrentService } from './app.current.service'
import { Router } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate { 
  
   constructor(
       private appCurrent: AppCurrentService,
       private router: Router
    ) {}; 

  canActivate() {
    var aux = this.appCurrent.isLogin();
    if (!aux){
        this.router.navigate(['login']);
    }
    return aux;
  }
}