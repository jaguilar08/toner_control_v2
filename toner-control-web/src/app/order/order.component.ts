import { Component, ViewChild } from '@angular/core';
import { TonerService } from '../toner.service'
import { CreateOrderComponent } from '../create-order/create.order.component';
import { HeaderComponent } from '../header/header.component'
import { TonerComponent } from '../toner/toner.component'
import { Router } from "@angular/router";
import { OrderViewComponent } from './view_order/order.view.component'
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { VerifyOrderViewComponent } from './view_verify_order/verify.order.view.component';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent {
  title = 'Order';
  title2 = 'Create order'
  aOrders = [];
  isLoad = false;
  order_number = null;
  mode_view = false;
  startDate = null;
  endDate = null;
  filter_order_number = null;
  Filter = {
    "start_date": null,
    "end_date": null,
    "order_number": null
  };
  private load_pending_verified: boolean = true;
  @ViewChild('header') header: HeaderComponent;
  @ViewChild('inventory') toner: TonerComponent;
  @ViewChild('createOrder') createOrder: CreateOrderComponent;
  @ViewChild('orderView') orderView: OrderViewComponent;
  @ViewChild('verifyOrder') verifyOrder: VerifyOrderViewComponent;

  constructor(private tService: TonerService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.endDate = moment().format('YYYY-MM-DD');
    this.startDate = moment().add(-90, 'day').format('YYYY-MM-DD');
    this.route.params.subscribe(params => {
      let number = +params['id'];
      let number2 = +params['verify'];
      if (number) {
        this.mode_view = true;
        this.order_number = number;
        this.load_order(number);
      }
      else {
        this.mode_view = false;
        if (number2){
          this.verify_order(number2);
        }
      }
    });
    // actualiza la lista si se hace nuevo registro
    this.createOrder.emitEvent.subscribe(res => {
      this.load_data();
      //this.toner.load_data();
    });
    this.verifyOrder.emitEvent.subscribe(res => {
      this.load_data();
      this.toner.load_data();
    })
    // actualiza el título
    this.createOrder.changeTitleEvent.subscribe(title => {
      this.title2 = title;
    });
    this.createOrder.loadData.subscribe(ok => {
      if (ok) {
        this.header.show_load();
      }
      else {
        this.header.hide_load();
      }
    });
    this.load_data();
  }

  // carga la orden
  load_order(order_number) {
    this.orderView.load_order(order_number);
  }

  verify_order(order_number){
    this.verifyOrder.load_order(order_number);
  }

  // cierra la orden
  closeView() {
    this.router.navigate(['/order']);
  }

  // carga los datos en lista
  load_data() {
    if (this.startDate == '') {
      this.toastr.error('Start date', 'Start date not selected');
      return;
    }
    if (this.endDate == '') {
      this.toastr.error('End date', 'End date not selected');
      return;
    }
    let args = {
      order_number: this.filter_order_number || 0,
      start_date: moment(this.startDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
      end_date: moment(this.endDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
      load_pending_verified: this.load_pending_verified
    };
    this.isLoad = true;
    this.tService.getLastOrder(args).subscribe(data => {
      this.aOrders = data;
      this.isLoad = false;
    });
  }
}