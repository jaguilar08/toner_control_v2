import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TonerService } from '../../toner.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmComponent } from '../../partials/dialog-confirm/confirm.component';

@Component({
  selector: 'dialog-view-verify-order',
  templateUrl: './view.verify.order.component.html',
  styleUrls: ['./view.verify.order.component.css']
})
export class ViewVerifiedOrder {

  private toner_order_verified_id: number = 0;
  private isLoad: boolean = false;
  private Order: any = {};

  constructor(
    public dialogRef: MatDialogRef<ViewVerifiedOrder>,
    private tService: TonerService,
    @Inject(MAT_DIALOG_DATA) public args: any) {
    this.toner_order_verified_id = args.toner_order_verified_id;
    this.load();
  }

  load() {
    this.isLoad = true;
    this.tService.getDetailVerifiedOrder(this.toner_order_verified_id).subscribe(data => {
      this.Order = data;
      this.isLoad = false;
    });
  }

  // aceptar
  accept() {
    this.dialogRef.close("accept");
  }
}
