import { Component } from '@angular/core';
import { TonerService } from '../../toner.service'

@Component({
    selector: 'app-order-view',
    templateUrl: './order.view.component.html',
    styleUrls: ['./order.view.component.css']
})
export class OrderViewComponent {

    isLoad = false;
    Order = {};

    constructor(
        private tService: TonerService
    ) { }

    load_order(order_number) {
        this.isLoad = true;
        this.Order = {
            "toners": []
        };
        this.tService.getOrderByNumber(order_number).subscribe(data => {
            this.Order = data;
            this.isLoad = false;
        });
    }
}