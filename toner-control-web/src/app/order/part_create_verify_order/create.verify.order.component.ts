import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TonerService } from '../../toner.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmComponent } from '../../partials/dialog-confirm/confirm.component';

@Component({
  selector: 'dialog-create-verify-order',
  templateUrl: './create.verify.order.component.html',
  styleUrls: ['./create.verify.order.component.css']
})
export class CreateVerifiedOrder {

  private toner_order_number: number = 0;
  private toner_order_id: number = 0;
  private isLoad: boolean = false;
  private isSaved: boolean = false;
  private Order: any = {};

  constructor(
    public dialogRef: MatDialogRef<CreateVerifiedOrder>,
    private tService: TonerService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public args: any) {
    this.toner_order_id = args.toner_order_id;
    this.toner_order_number = args.toner_order_number;
    this.load();
  }

  load() {
    this.isLoad = true;
    this.tService.getOrderByNumber(this.toner_order_number).subscribe(data => {
      this.Order = data;
      let toners = [];
      data.toners.forEach(item => {
        item.received_quantity = 0;
        if (item.pending_quantity > 0){
          toners.push(item);
        }
      });
      this.Order.toners = toners;
      this.isLoad = false;
    });
  }

  complete(item:any){
    item.received_quantity = item.pending_quantity;
  }

  // aceptar
  accept() {
    let aux = 0; // suma todas los valores introducidos por teclado
    let bandera: boolean = true;
    if (this.isLoad) {  //si esta cargando
      return;
    }
    this.Order.toners.forEach(item => {
      aux += item.received_quantity;
      if (item.received_quantity < 0) { // si la cantidad ingresada es negativa
        this.toastr.warning('Error', 'Invalid value');
        bandera = false;
        return;
      }
      if (item.pending_quantity < item.received_quantity) {  // si la cantidad ingresada es mayor que la pendiente
        this.toastr.warning('Error', 'The received value can not be greater than the slope');
        bandera = false;
        return;
      }
    });
    if (aux <= 0) {
      this.toastr.warning('Error', 'You are not specifying any amount');
      return;
    }
    if (!bandera) {  // si bandera es false no ejecuta el guardado
      return;
    }
    this.isSaved = true;
    this.isLoad = true;
    let args = {
      toner_order_number: this.Order.number,
      toner_order_id: this.Order.id,
      toners: this.Order.toners,
    }
    this.tService.verified_order(args).subscribe(data => {
      this.dialogRef.close("accept");
      this.isSaved = false;
      this.isLoad = false;
    }, err => {
      this.toastr.warning('Error', 'An error occurred while saving');
      this.isSaved = false;
      this.isLoad = false;
    });
  }

  // cancelar
  cancel() {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { title: 'Confirm', message: '¿Sure you want to cancel the verification?' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'accept') {
        this.dialogRef.close("cancel");
      }
    });

  }
}
