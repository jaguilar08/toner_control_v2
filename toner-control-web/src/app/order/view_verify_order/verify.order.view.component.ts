import { Component, Output, EventEmitter } from '@angular/core';
import { TonerService } from '../../toner.service'
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ConfirmComponent } from '../../partials/dialog-confirm/confirm.component';
import { CreateVerifiedOrder } from '../part_create_verify_order/create.verify.order.component';
import { ViewVerifiedOrder } from '../part_view_verify_order/view.verify.order.component';

@Component({
    selector: 'app-verify-order-view',
    templateUrl: './verify.order.view.component.html',
    styleUrls: ['./verify.order.view.component.css']
})
export class VerifyOrderViewComponent {

    @Output() emitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    private showSelectOrder: boolean = true;
    private isLoad: boolean = false;
    private pending_verified: boolean = null;
    private toner_order_id: number = 0;
    private toner_order_number: number = 0;
    private aVerified: Array<any> = [];

    constructor(
        private tService: TonerService,
        private router: Router,
        private toastr: ToastrService,
        private dialog: MatDialog,
    ) { }

    load_order(order_number) {
        this.toner_order_number = order_number;
        this.showSelectOrder = false;
        this.isLoad = true;
        this.tService.getAllVerifiedOrder(order_number).subscribe(data => {
            this.toner_order_id = data.toner_order_id;
            this.aVerified = data.list;
            this.pending_verified = data.pending_verified;
            this.isLoad = false;
        });
    }

    // atras
    back() {
        this.router.navigate(['order']);
    }

    viewVerified(toner_order_verified_id: number) {
        this.dialog.open(ViewVerifiedOrder, {
            width: '650px',
            data: { toner_order_verified_id: toner_order_verified_id, message: '¿Sure you want to cancel the verification?' }
        });
    }

    // crea verificacion
    createVerified() {
        if (this.pending_verified) {
            let dialogRef = this.dialog.open(CreateVerifiedOrder, {
                width: '650px',
                data: { toner_order_id: this.toner_order_id, toner_order_number: this.toner_order_number }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result == 'accept') {
                    this.toastr.success('Success', 'Saved verification');
                    this.emitEvent.emit(true);
                    this.load_order(this.toner_order_number);
                }
            });
        }
        else {
            this.toastr.warning('Success', 'The order is verified');
        }
    }
}