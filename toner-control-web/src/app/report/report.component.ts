import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TonerService } from '../toner.service'
import { AppCurrentService } from '../app.current.service'
import { HeaderComponent } from '../header/header.component'
import { Chart } from 'chart.js';
import * as moment from 'moment';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.css']
})
export class ReportComponent {
    @ViewChild('header') header: HeaderComponent;
    @ViewChild('canvas') canvas: ElementRef;
    dealershipControl = new FormControl('', [Validators.required]);
    printerControl = new FormControl('', [Validators.required]);
    tonerControl = new FormControl('', [Validators.required]);
    chart = [];
    aSubnets = [];
    aToners = [];
    selectedSubnet = null;
    isLoadFirstReport = false;
    startDate = null;
    endDate = null;

    fTonerInstalled = {
        'subnet_id': null,
        'aPrinters': [],
        'printer_id': 0,
        'toner_id': 0,
        'load_report': false,
        'start_date': moment().add(-15, 'day').format('YYYY-MM-DD'),
        'end_date': moment().format('YYYY-MM-DD'),
        'report_data': []
    };

    constructor(
        private tService: TonerService,
        private toastr: ToastrService,
        private appCurrent: AppCurrentService
    ) {
    }

    ngAfterViewInit(): void {
        this.endDate = moment().format('YYYY-MM-DD');
        this.startDate = moment().add(-90, 'day').format('YYYY-MM-DD');
        this.tService.getToner().subscribe(data => {
            this.aToners = data;
        });
        this.tService.getAllSubnet().subscribe(data => {
            this.aSubnets = data;
            if (data.length > 0) {
                this.selectedSubnet = data[0].id;
                this.reportChangeTonerForDealerhip();
                //report 1
                this.fTonerInstalled.subnet_id = data[0].id;
                this.tonerReportInstalled();
                this.onSelectSubnetChange();
            }
        });
    }

    onSelectSubnetChange() {
        let aux = this.fTonerInstalled;
        aux.aPrinters = [];
        aux.printer_id = 0;
        if (aux.subnet_id > 0) {
            this.tService.getPrintersForSubnet(aux.subnet_id).subscribe(data => {
                aux.aPrinters = data;
            });
        }
    }

    // reporte 
    tonerReportInstalled() {
        let aux = this.fTonerInstalled;
        if (aux.subnet_id == null) {
            this.toastr.error('Dealership', 'Dealership not selected');
            return;
        }
        if (aux.start_date == null) {
            this.toastr.error('Start date', 'Start date not selected');
            return;
        }
        if (aux.end_date == null) {
            this.toastr.error('End date', 'End date not selected');
            return;
        }
        let args = {
            'start_date': moment(aux.start_date, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
            'end_date': moment(aux.end_date, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
            'subnet_id': aux.subnet_id,
            'printer_id': aux.printer_id,
            'toner_id': aux.toner_id
        };
        aux.load_report = true;
        this.tService.getReportPrinterInstalled(args).subscribe(data => {
            aux.report_data = data;  // datos del reporte
            aux.load_report = false;
        }, err => {
            console.log(err);
        });
    }

    sendReport() {
        this.header.show_load();
        let aux = this.fTonerInstalled;
        let args = {
            'subnet': aux.subnet_id == 0 ? 'All' : this.aSubnets.find(a => a.id == aux.subnet_id).name,
            'printer': aux.printer_id == 0 ? 'All' :  aux.aPrinters.find(a => a.id == aux.printer_id).name,
            'toner': aux.toner_id == 0 ? 'All' : this.aToners.find(a => a.id == aux.toner_id).name,
            'start_date': aux.start_date,
            'end_date': aux.end_date,
            'ccs': [this.appCurrent.getCurrentEmailUser()],
            'toners': []
        }
        aux.report_data.forEach(item => {
            let _date = moment.utc(item.instalation_date, "YYYY-MM-DD HH:mm:ss").toDate();
            item.instalation_date = moment(_date).local().format("MMMM DD YYYY, h:mm a");
            args.toners.push(item);
        });
        aux.load_report = true;
        this.tService.sendReportPrinterInstalled(args).subscribe(data => {
            aux.load_report = false;
            this.header.hide_load();
        }, err => {
            console.log(err);
            this.header.hide_load();
        });
    }

    // reporte
    reportChangeTonerForDealerhip() {
        if (this.selectedSubnet == null) {
            this.toastr.error('Dealership', 'Dealership not selected');
            return;
        }
        if (this.startDate == null) {
            this.toastr.error('Start date', 'Start date not selected');
            return;
        }
        if (this.endDate == null) {
            this.toastr.error('End date', 'End date not selected');
            return;
        }
        let args = {
            'start_date': moment(this.startDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
            'end_date': moment(this.endDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
            'subnet_id': this.selectedSubnet
        };
        this.isLoadFirstReport = true;
        this.tService.getReportPrinter(args).subscribe(data => {
            let cx = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');
            this.chart = new Chart(cx, {
                type: 'bar',
                data: {
                    labels: data.labels,
                    datasets: data.datasets,
                },
                options: {}
            });
            this.isLoadFirstReport = false;
        }, err => {
            console.log(err);
        });
    }

}

