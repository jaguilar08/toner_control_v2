import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { TonerService } from '../../toner.service'

@Component({
    selector: 'app-inventory-backup',
    templateUrl: './inventory.backup.component.html',
    styleUrls: ['./inventory.backup.component.css']
})
export class IventoryBackupComponent {
    dealershipControl = new FormControl('', [Validators.required]);
    selectedDealership = null;
    isLoad = false;
    aDealerships = [];
    aToners = [];

    constructor(
        private tService: TonerService
    ) { }

    ngOnInit() {
        this.isLoad = true;
        this.tService.getAgency().subscribe(data => {
            this.aDealerships = data;
            this.isLoad = false;
            if (this.aDealerships && this.aDealerships.length > 0) {
                this.selectedDealership = this.aDealerships[0].id;
                this.onSelectDealershipChange();
            }
        });
    }

    onSelectDealershipChange() {
        this.load_data();
    }

    load_data() {
        this.isLoad = true;
        this.tService.getInventoryByAgency(this.selectedDealership).subscribe(data => {
            this.aToners = data;
            this.isLoad = false;
        });
    }
}