import { Component } from '@angular/core';
import { TonerService } from '../../toner.service'

@Component({
  selector: 'app-view-assignment',
  templateUrl: './view.assignment.component.html',
  styleUrls: ['./view.assignment.component.css']
})
export class ViewAssignmentComponent{

    isLoad = false;
    Order = {};

    constructor(
        private tService: TonerService
    ) { }

    load_order(order_number) {
        this.isLoad = true;
        this.Order = {
            "printers": []
        };
        this.tService.getAssignmentByNumber(order_number).subscribe(data => {
            this.Order = data;
        }, err => {

        }, () => {
            this.isLoad = false;
        });
    }
}