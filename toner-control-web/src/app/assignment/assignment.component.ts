import { Component, ViewChild } from '@angular/core';
import { TonerService } from '../toner.service'
import { HeaderComponent } from '../header/header.component'
import { CreateAssignmentComponent } from './create_assignment/create.assignment.component'
import { ViewAssignmentComponent } from './view_assignment/view.assignment.component'
import { IventoryComponent } from './inventory/inventory.component'
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css']
})
export class AssignmentComponent {
  @ViewChild('header') header: HeaderComponent;
  @ViewChild('create') create: CreateAssignmentComponent;
  @ViewChild('viewAssignment') viewAssignment: ViewAssignmentComponent;
  @ViewChild('inventory') inventory: IventoryComponent;
  aAssignment = [];
  isLoadLastAssignment = false;
  isLoadTonerNotAssignment = false;
  isLoadTonerToChange = false;
  aTonersNoMatch = [];
  aTonersToChange = [];
  aSubnets = [];
  selectedSubnet = null;
  startDate = null;
  endDate = null;
  order_number = null;
  mode_view = false;

  constructor(
    private tService: TonerService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.endDate = moment().format('YYYY-MM-DD');
    this.startDate = moment().add(-20, 'day').format('YYYY-MM-DD');
    this.route.params.subscribe(params => {
      let number = +params['id'];
      if (number) {
        this.mode_view = true;
        this.order_number = number;
        this.load_order(number);
      }
      else {
        this.mode_view = false;
      }
    });
    //al crear el registro
    this.create.create.subscribe(ok => {
      if(ok){
        this.load_data();  // carga los ultimos registros
        this.inventory.load_data();  // carga el inventario
        this.load_not_match(); // carga los toner no contrados
        this.load_toner_to_change(this.selectedSubnet); // carga los toner a cambiar
      }
    });
    this.isLoadTonerToChange = true;
    this.tService.getAllSubnet().subscribe(data => {
      this.aSubnets = data;
      this.isLoadTonerToChange = false;
      if (data.length > 0){
        this.load_toner_to_change(data[0].id);
      }
    });
    // mostrar el spinner
    this.create.showLoad.subscribe(s => {
      if (s) {
        this.header.show_load();
      }
      else {
        this.header.hide_load();
      }
    });
    this.load_data();
    this.load_not_match();
    //this.load_toner_to_change();
  }

  // carga la orden
  load_order(order_number) {
    this.viewAssignment.load_order(order_number);
  }

  // cierra la orden
  closeView() {
    this.router.navigate(['/assignment']);
  }

  // carga la lista
  load_data() {
    if (this.startDate == '') {
      this.toastr.error('Start date', 'Start date not selected');
      return;
    }
    if (this.endDate == '') {
      this.toastr.error('End date', 'End date not selected');
      return;
    }
    let args = {
      'start_date': moment(this.startDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD'),
      'end_date': moment(this.endDate, 'YYYY-MM-DD').utc().format('YYYY-MM-DD')
    };
    this.isLoadLastAssignment = true;
    this.tService.getLastAssignment(args).subscribe(data => {
      this.aAssignment = data;
      this.isLoadLastAssignment = false;
    });
  }

  load_not_match(){
    this.isLoadTonerNotAssignment = true;
    this.tService.getTonerNoAssignment().subscribe(data =>{
      this.aTonersNoMatch = data;
      this.isLoadTonerNotAssignment = false;
    })
  }

  // carga los toner a cambiar
  load_toner_to_change(subnet_id){
    this.selectedSubnet = subnet_id;
    this.isLoadTonerToChange = true;
    this.tService.getTonerToChange(subnet_id).subscribe(data => {
      this.aTonersToChange = data;
      this.isLoadTonerToChange = false;
    });
  }
}