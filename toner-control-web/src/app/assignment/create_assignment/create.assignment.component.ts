import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TonerService, Subnet } from '../../toner.service'
import { ConfirmComponent } from '../../partials/dialog-confirm/confirm.component'
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-create-assignment',
  templateUrl: './create.assignment.component.html',
  styleUrls: ['./create.assignment.component.css']
})
export class CreateAssignmentComponent {
  @Output() create: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() showLoad: EventEmitter<boolean> = new EventEmitter<boolean>();

  aDealerships = [];
  aPrinters: Subnet[];
  aToners = [];
  selectedDealership = '';
  isSaveDisabled = false;  // congela el boton guardar 
  isLoadToners = false; // carga los toner
  isLoadDealership = false;  // carga dealership
  isLoadPrinter = false;  // carga impresora
  dealershipControl = new FormControl('', [Validators.required]);
  printerControl = new FormControl('', [Validators.required]);

  editItem = false;
  newItem = {
    "printer_id": null,
    "printer_name": null,
    "toners": []
  };

  Items = [];

  constructor(
    private tService: TonerService,
    private toastr: ToastrService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.isLoadDealership = true;
    this.tService.getAgency().subscribe(data => {
      this.aDealerships = data;
      this.isLoadDealership = false;
    });
    this.isLoadToners = true;
    this.tService.getToner().subscribe(data => {
      data.forEach(t => t.quantity = 0);
      this.aToners = data;
      this.init_new_item();
    }, err => {

    }, () => {
      this.isLoadToners = false;
    });
  }

  // inicializa el nuevo item
  init_new_item() {
    this.newItem.printer_id = null;
    this.newItem.printer_name = null;
    this.newItem.toners = [];
    this.aToners.forEach(toner => {
      this.newItem.toners.push({
        "toner_id": toner.id,
        "name": toner.name,
        "backup_quantity": 0,
        "replaced": false
      });
    });
  }

  // obtiene el nombre de impresora por el identificador
  getNamePrinter(printer_id) {
    var printer_name = null;
    /*this.aPrinters.forEach(printer => {
      if (printer.id == printer_id) {
        printer_name = printer.name;
      }
    });*/
    this.aPrinters.forEach(subnet => {
      subnet.printers.forEach(printer => {
        if (printer.id == printer_id) {
          printer_name = printer.name;
        }
      });
    });
    return printer_name;
  }

  // agrega item
  add_printer() {
    if (this.newItem.printer_id == null) {
      this.toastr.error('Printer', 'Printer not selected');
      return;
    }
    var currentItem = this.Items.find(a => a.printer_id == this.newItem.printer_id); // busca item
    if (!currentItem) {
      let toners = [];
      this.aToners.forEach(toner => {
        toners.push({
          "toner_id": toner.id,
          "name": toner.name,
          "backup_quantity": 0,
          "replaced": false
        });
      });
      currentItem = {
        "printer_id": this.newItem.printer_id,
        "printer_name": this.getNamePrinter(this.newItem.printer_id),
        "toners": toners
      };
      this.Items.push(currentItem);
    }
  }

  // actualiza los contadores del resumen
  updateResume(toner_id) {
    let toner = this.aToners.find(a => a.id == toner_id);
    toner.quantity = 0;
    this.Items.forEach(item => {
      item.toners.forEach(aux => {
        if (aux.toner_id == toner_id) {
          if (aux.replaced) {
            toner.quantity++;
          }
          toner.quantity += aux.backup_quantity;
        }
      });
    });
  }

  updateResumenAllToner() {
    this.aToners.forEach(toner => {
      this.updateResume(toner.id);
    });
  }

  //edita
  edit_printer(item) {
    this.newItem.printer_id = item.printer_id;
    this.newItem.printer_name = item.printer_name;
    item.toners.forEach(aux => {
      this.newItem.toners.forEach(toner => {
        if (aux.toner_id == toner.toner_id) {
          toner.name = aux.name;
          toner.serial = aux.serial;
          toner.replaced = aux.replaced;
        }
      });
    });
    this.editItem = true;
  }

  // elimina de la lista
  delete_printer(item) {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { title: 'Confirm', message: '¿You are sure you want to delete the item from the list?' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'accept') {
        let index = this.Items.findIndex(aux => aux.printer_id == item.printer_id);
        if (index > -1) {
          this.Items.splice(index, 1);
          this.updateResumenAllToner();  // actualiza los contadores
        }
      }
    });
  }

  // al seleccionar el pbx carga todas las impresoras
  onSelectPBXChange() {
    this.isLoadPrinter = true;
    this.tService.getPrintersForAgency(this.selectedDealership).subscribe(data => {
      let items = [];
      items.push({
        "id": 0,
        "name": "Backup",
        "printers": [{
          "id": 0,
          "name": 'Backup'
        },]
      });
      data.forEach(item => items.push(item));
      this.aPrinters = items;
      this.isLoadPrinter = false;
    }, err => {
      this.isLoadPrinter = false;
    }, () => {
      this.isLoadPrinter = false;
    });
  }

  // registra nuevo 
  add_assignment() {
    let items = [];
    this.Items.forEach(printer => {
      let aux = {
        "printer_id": printer.printer_id,
        "toners": []
      };
      printer.toners.forEach(toner => {
        if (toner.replaced || toner.backup_quantity > 0) {
          aux.toners.push(toner);
        }
      });
      if (aux.toners.length > 0) {
        items.push(aux);
      }
    });
    if (items.length == 0) {
      this.toastr.error('Error', 'Unassigned toners');
      return;
    }
    var args = {
      'agency_id': this.selectedDealership,
      'printers': items
    };
    this.showLoad.emit(true);
    this.tService.addAssignment(args).subscribe(response => {
      this.toastr.success('Success', 'Saved order');
      this.Items = [];
      this.aToners.forEach(t => t.quantity = 0);
      this.showLoad.emit(false);
      this.create.emit(true);  // creo el registro
    }, err => {
      this.showLoad.emit(false);
      this.toastr.error('Error', err.error.message);
    });
  }
}