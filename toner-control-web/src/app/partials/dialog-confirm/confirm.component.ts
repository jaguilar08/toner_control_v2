import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'dialog-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent {

  title = "Confirm"
  message = '¿Are you sure?';

  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public args: any) {
    this.title = args.title;
    this.message = args.message;
  }

  // aceptar
  accept() {
    this.dialogRef.close("accept");
  }

  // cancelar
  cancel() {
    this.dialogRef.close("cancel");
  }
}