import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppCurrentService } from './app.current.service';

export class Toner {
  constructor(
    public id: number,
    public name: string,
    public quantity: number,
    public received_quantity: number,
    public pending_quantity: number) { }
}

export class OrderToner {
  constructor(
    public number: string,
    public post_date: string
  ) { }
}

export class InfoOrderToner {
  constructor(
    public number: string,
    public order_date: string,
    public post_date: string,
    public toners: Toner[]
  ) { }
}

export class Assignment {
  constructor(
    public id: number,
    public agency: string,
    public post_date: string
  ) { }
}

export class Dealership {
  constructor(
    public id: number,
    public name: string
  ) { }
}

export class Printer {
  constructor(
    public id: number,
    public name: string,
    public SelectedPrinters: number[]
  ) { }
}

export class Subnet {
  constructor(
    public id: number,
    public name: string,
    public printers: Printer[]
  ) { }
}

@Injectable()
export class TonerService {

  @Output() beginLoadData: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() loadData: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private http: HttpClient,
    private appCurrent: AppCurrentService
  ) { }

  readonly url_api = "http://173.235.74.153:8015/toner/"; //"http://173.235.74.153:8015/toner/";

  authentication(username, password) {
    var args = {
      'device_id': 3, // web
      'username': username,
      'password': password
    }
    return this.post('authentication', args);
  }

  // crea el header de la peticion http
  private createHeader() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'security-token': this.appCurrent.getToken()
      })
    };
  }

  // method post
  private post<T>(method, args): any {
    //this.beginLoadData.emit(true);
    var aux = this.http.post<T>(this.url_api + method, args, this.createHeader());
    //aux.subscribe(ok => { }, err => { }, () => { console.log('termina'); this.loadData.emit(false); });
    return aux;
  }

  // method get
  private get<T>(method) {
    var aux = this.http.get<T>(this.url_api + method, this.createHeader());
    //aux.subscribe(ok => { }, err => { }, () => { console.log('termina'); this.loadData.emit(false); });
    return aux;
  }

  // desloguea al usuario
  logout() {
    return this.get('logout');
  }

  getHome(agency_id) {
    let args = {
      'subnet_id': agency_id
    };
    return this.post('get_home', args);
  }

  getHomeResume() {
    //get_home_resume
    return this.get<any[]>('get_home_resume');
  }

  // obtiene los toner
  getToner() {
    return this.get<Toner[]>('get_toners');
  }

  // Obtiene todas las ordenes 
  getLastOrder(args) {
    return this.post<OrderToner[]>('get_last_order', args);
  }

  addOrder(args) {
    args["toner_user_id"] = this.appCurrent.getCurrentUserID();
    return this.post('add_order', args);
  }

  // obtiene order por el número
  getOrderByNumber(number) {
    return this.get<InfoOrderToner>('get_order_by_number?order_number=' + number);
  }

  // obtiene todas las agencias
  getAgency() {
    return this.get<Dealership[]>('get_all_agency');
  }

  getAllSubnet() {
    return this.get<any[]>('get_all_subnet');
  }

  // obtiene todas las impresoras para la agencia
  getPrintersForAgency(agency_id) {
    return this.get<Printer[]>('get_printers_for_agency?agency_id=' + agency_id);
  }

  getPrintersForSubnet(subnet_id) {
    return this.get<Printer[]>('get_printers_for_subnet?subnet_id=' + subnet_id);
  }

  // obtiene las ultimas asignaciones realizadas
  getLastAssignment(args) {
    return this.post<Assignment[]>('get_last_assignment', args);
  }

  addAssignment(args) {
    args["toner_user_id"] = this.appCurrent.getCurrentUserID();
    return this.post('add_assignment', args);
  }

  getAssignmentByNumber(assignment_number) {
    return this.get<any>("get_assignment_by_number?assignment_number=" + assignment_number);
  }

  getTonersNoMatch() {
    return this.get<any[]>('get_assignment_toner_no_match');
  }

  getInventoryByAgency(agency_id) {
    let args = {
      "agency_id": agency_id
    };
    return this.post<any[]>('get_inventory_by_agency', args);
  }

  getTonerNoAssignment() {
    return this.get<any[]>('get_toner_no_assignment');
  }

  // toner que deben ser cambiados
  getTonerToChange(subnet_id) {
    let args = {
      "subnet_id": subnet_id
    };
    return this.post<any[]>('toner_to_change', args);
  }

  getReportPrinter(args) {
    return this.post<any[]>('get_report_printer', args);
  }

  getReportPrinterInstalled(args) {
    return this.post<any[]>('get_report_printer_installed', args);
  }

  sendReportPrinterInstalled(args) {
    return this.post<any>('send_report_printer_installed', args);
  }

  getInventoryResidualQuantity(agency_id) {
    let args = {
      "agency_id": agency_id
    };
    return this.post<any[]>('get_inventory_residual_quantity', args);
  }

  addResidualToner(args) {
    args["toner_user_id"] = this.appCurrent.getCurrentUserID();
    return this.post('add_residual_toner', args);
  }

  getLastResidualToner(args) {
    return this.post<any[]>('get_last_residual_toner', args);
  }

  getResidualByNumber(residual_id) {
    return this.get<any>('get_residual_by_number?residual_number=' + residual_id);
  }

  verified_order(args) {
    args.toner_user_id = this.appCurrent.getCurrentUserID();
    return this.post<any>('verified_order', args);
  }

  getAllVerifiedOrder(order_number: number) {
    let args = {
      order_number: order_number
    };
    return this.post<any>('get_all_verified_order', args);
  }

  getDetailVerifiedOrder(toner_order_verified_id: number) {
    let args = {
      toner_order_verified_id: toner_order_verified_id
    };
    return this.post<any>('get_detail_verified_order', args);
  }
}