import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

export class CurrentUser {
    constructor(
        public username: string
    ) { }
}


@Injectable()
export class AppCurrentService {

    readonly credential: string = "credential";
    readonly tokenSecurity: string = "security-token";
    readonly remember: string = "remember";

    constructor(
        private cookie: CookieService
    ) { }

    // asigna credenciales
    setCredential(dataUser, remember) {
        var cookieExp = new Date();
        cookieExp.setDate(cookieExp.getDate() + 30);
        var options = { expires: cookieExp };  // opciones
        this.cookie.put(this.remember, remember, options);
        this.cookie.put(this.tokenSecurity, dataUser[this.tokenSecurity], options);
        this.cookie.putObject(this.credential, dataUser, options);
    }

    // verifica si esta marcado para recordar la sesión
    isRemember() {
        var aux = this.cookie.get(this.remember);
        if (aux){
            return aux == 'true';
        }
        return false;
    }

    // devuelve un valor si ya está logueado el usuario
    isLogin() {
        return this.cookie.get(this.tokenSecurity) != null;//this.cookie.check(this.credential);
    }

    // obtiene el token
    getToken() {
        return this.cookie.get(this.tokenSecurity) || '';
    }

    getCurrentUserID() {
        return this.cookie.getObject(this.credential)['d_user']['toner_user_id'] || '';
    }

    getCurrentEmailUser(){
        return this.cookie.getObject(this.credential)['d_user']['email'] || '';
    }

    getCredential(){
        return this.cookie.getObject(this.credential);
    }

    // elimina las credenciales guardadas en la cookie
    clearCredential() {
        this.cookie.remove(this.credential);
        this.cookie.remove(this.tokenSecurity);
    }
}