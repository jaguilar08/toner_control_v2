import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import { TonerComponent } from './toner/toner.component';
import { OrderComponent } from './order/order.component';
import { VerifyOrderViewComponent } from './order/view_verify_order/verify.order.view.component'
import { OrderViewComponent } from './order/view_order/order.view.component'
import { CreateOrderComponent } from './create-order/create.order.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { CreateAssignmentComponent } from './assignment/create_assignment/create.assignment.component';
import { ViewAssignmentComponent } from './assignment/view_assignment/view.assignment.component'
import { IventoryComponent } from './assignment/inventory/inventory.component';
import { IventoryBackupComponent } from './assignment/inventory-backup/inventory.backup.component'
import { ReportComponent } from './report/report.component'
import { ResidualTonerComponent } from './residual/residual.component'
import { CreateResidualTonerComponent } from './residual/create-residual/create.residual.component'
import { ResidualInventoryTonerComponent } from './residual/residual-inventory/residual.inventory.component'
import { ViewResidualTonerComponent } from './residual/view-residual/view.residual.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { TonerService } from './toner.service'
import { AppCurrentService } from './app.current.service'
import { AuthGuard } from './AuthGuard'
import { CookieModule } from 'ngx-cookie';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ReadableDatePipe, fmtDatePipe } from './readableDate';
import { TimeAgoPipe } from 'time-ago-pipe';

//material
import { MatButtonModule, MatCheckboxModule, MatSelectModule, MatInputModule, MatExpansionModule, MatListModule, MatProgressSpinnerModule, MatCardModule, MatTooltipModule, MatTableModule, MatGridListModule } from '@angular/material';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from "@angular/material";
import { ConfirmComponent } from "./partials/dialog-confirm/confirm.component";
import { CreateVerifiedOrder } from "./order/part_create_verify_order/create.verify.order.component";
import { ViewVerifiedOrder } from './order/part_view_verify_order/view.verify.order.component'

const ROUTES: Route[] = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'toner', component: TonerComponent, canActivate: [AuthGuard] },
  { path: 'order', component: OrderComponent, canActivate: [AuthGuard] },
  { path: 'order/:id', component: OrderComponent, canActivate: [AuthGuard] },
  { path: 'order/verify/:verify', component: OrderComponent, canActivate: [AuthGuard] },
  { path: 'assignment', component: AssignmentComponent, canActivate: [AuthGuard] },
  { path: 'assignment/:id', component: AssignmentComponent, canActivate: [AuthGuard] },
  { path: 'residual', component: ResidualTonerComponent, canActivate: [AuthGuard] },
  { path: 'residual/:id', component: ResidualTonerComponent, canActivate: [AuthGuard] },
  { path: 'report', component: ReportComponent, canActivate: [AuthGuard] }
]

@NgModule({
  declarations: [
    LoginComponent,
    HeaderComponent,
    HomeComponent,
    AppComponent,
    TonerComponent,
    OrderComponent,
    OrderViewComponent,
    VerifyOrderViewComponent,
    CreateOrderComponent,
    AssignmentComponent,
    CreateAssignmentComponent,
    ViewAssignmentComponent,
    ConfirmComponent,
    CreateVerifiedOrder,
    ViewVerifiedOrder,
    IventoryComponent,
    IventoryBackupComponent,
    ReportComponent,
    ReadableDatePipe,
    fmtDatePipe,
    TimeAgoPipe,
    ResidualTonerComponent,
    CreateResidualTonerComponent,
    ResidualInventoryTonerComponent,
    ViewResidualTonerComponent
  ],
  imports: [
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule, MatCheckboxModule, MatDialogModule, MatSelectModule, MatInputModule, MatExpansionModule, MatListModule, MatProgressSpinnerModule, MatCardModule, MatTooltipModule, MatTableModule, MatGridListModule, // material
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    CookieModule.forRoot(),
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    TonerService,
    AppCurrentService,
    AuthGuard,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }
  ],
  entryComponents: [ConfirmComponent, CreateVerifiedOrder, ViewVerifiedOrder],
  bootstrap: [AppComponent]
})
export class AppModule {
}
