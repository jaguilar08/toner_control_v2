import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { TonerService, Dealership, Printer } from '../toner.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  dealershipControl = new FormControl('', [Validators.required]);
  aDealerships = [];
  selectedDealership = '';
  aPrinters = [];
  dealership = 'Select a dealership';
  isLoad = false;
  isResume = false;

  constructor(
    private tService: TonerService
  ) { }

  ngOnInit() {
    this.tService.getAllSubnet().subscribe(data => {
      this.aDealerships = data;
      /*if (data.length > 0) {
        this.load_printer(data[0]);
      }*/
    });
    this.load_resume();
  }

  load_resume() {
    this.isResume = true;
    this.dealership = "Resume";
    this.isLoad = true;
    this.aPrinters = [];
    this.tService.getHomeResume().subscribe(data => {
      this.aPrinters = data;
      this.isLoad = false;
    });
  }

  load_printer(subnet) {
    this.isResume = false;
    this.dealership = subnet.name;
    this.isLoad = true;
    this.aPrinters = [];
    this.tService.getHome(subnet.id).subscribe(data => {
      this.aPrinters = data;
      this.isLoad = false;
    });
  }
}
