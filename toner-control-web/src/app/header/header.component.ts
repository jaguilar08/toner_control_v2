import { Component, OnInit } from '@angular/core';
import { AppCurrentService } from '../app.current.service';
import { ToastrService } from 'ngx-toastr';
import { TonerService } from '../toner.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfirmComponent } from '../partials/dialog-confirm/confirm.component'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  value_to_hide: number = 0;
  last_token: string = '';
  UserLoggedIn: string = '';
  token: string = '';

  constructor(
    private appCurrent: AppCurrentService,
    private toastr: ToastrService,
    private router: Router,
    private dialog: MatDialog,
    private tonerServ: TonerService,
    private spinner: NgxSpinnerService
  ) {
      
  }

  ngOnInit() {
    this.token = this.appCurrent.getToken();
    this.UserLoggedIn = this.appCurrent.getCredential()['d_user']['name'];
  }

  showWarning(){
    this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { title: 'Info', message: 'Sorry, at this time it is not possible to change your username and password.' }
    });
  }

  // muestra el spinner
  show_load() {
    //this.value_to_hide += 1;
    this.spinner.show();
  }

  // oculta el spinner
  hide_load() {
    /*this.value_to_hide -= this.value_to_hide > 0 ? 1 : 0;
    if (this.value_to_hide == 0){
      this.spinner.hide();
    }*/
    this.spinner.hide();
  }

  session_close() {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { title: 'Confirm', message: '¿Sure you want to close the session?' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'accept') {
        this.tonerServ.logout().subscribe(response => {
          this.appCurrent.clearCredential();
          this.router.navigate(['login']);
        }, error => {
          this.toastr.error('Error', 'UPS! An error has occurred...');
        });
      }
    });
  }
}