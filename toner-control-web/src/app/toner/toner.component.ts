import { Component, ViewChild } from '@angular/core';
import { TonerService } from '../toner.service'

@Component({
  selector: 'app-toner',
  templateUrl: './toner.component.html',
  styleUrls: ['./toner.component.css']
})
export class TonerComponent {
  title = 'Toner control';
  aToner = [];
  isLoad = false;

  constructor(private tService: TonerService) { }

  ngOnInit() {
    this.load_data();
  }

  // carga la información de inventario
  load_data() {
    this.isLoad = true;
    this.tService.getToner().subscribe(data => {
      this.aToner = data;
    }, err => {

    }, () => {
      this.isLoad = false;
    });
  }
}
