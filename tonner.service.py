# encoding=utf8
import requests
from bs4 import BeautifulSoup
import re
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import threading
import datetime
import sys

# 'http://173.235.74.153:8015/toner/' http://localhost:5003/
URL_SERVER = 'http://173.235.74.153:8015/toner/'
SUPPORT_EMAIL = "jaguilar@dealergeek.com"
PASSWORD = "aguilar08"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT = 587
MIN_VALUE_PERCENT = 15  # bajo de este valor envia email
threads = []  # hilos
inventories = []  # inventarios
subnets = [
    {
        'name': 'Ford',
        'subnet': '10.1.2',
        'printers': []
    },
    {
        'name': 'Accounting',
        'subnet': '10.2.1',
        'printers': []
    },
    {
        'name': 'Auto Credit',
        'subnet': '10.4.2',
        'printers': []
    },
    {
        'name': 'Toyota',
        'subnet': '10.6.2',
        'printers': []
    }
]

day_of_week = datetime.date.today().strftime("%A")
# cuando sea lunes o viernes se enviará un reporte completo
full_report = day_of_week == 'Monday' or day_of_week == 'Friday'


def get_inventory():
    """Obtiene el inventario"""
    method = "get_all_inventory"
    response = requests.request("GET", URL_SERVER + method)
    for row in json.loads(response.text):
        inventories.append(row)
    response.close()
    print(inventories)


def get_value(text):
    aux = text.replace(u"\xa0", u" ").replace('\\n', '')
    return aux.encode('ascii', 'ignore').decode('utf-8')


def __create_html(result):
    """Crea la estructura HTML"""
    pase = False
    for item in result:
        if len(item['printers']) > 0:
            pase = True
            break
    if not pase:
        return None
    aux = """ \
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <style>
        .subnet {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .subnet td, .subnet th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .subnet tr:nth-child(even){background-color: #f2f2f2;}

        .subnet tr:hover {background-color: #ddd;}

        .subnet th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4c61af;
            color: white;
        }
        </style>
    </head>
    """
    aux = """ \
        {0}
        <body>
        <h1>{1}</h1>
        <br />
        <div>
    """.format(aux, 'Full toner report' if full_report else 'Low toner report')
    # 'Full toner report' if full_report else 'Low toner report'
    for subnet in result:
        if len(subnet['printers']) > 0:
            aux_subnet = """ \
                <br />
                <h3>{0}</h3>
                <br />
                <table class="subnet">
                     <tr>
                        <th>Number</th>
                        <th>Description</th>
                        <th>Tonner</th>
                    </tr>
                """.format(subnet['name'])
            for row in subnet['printers']:
                aux_tonner = """ \
                    <ul>
                """
                for tonner in row['toner_low']:
                    aux_tonner = """ \
                        {0}
                        <li>
                            <div><small>{1}</small><b>{2}</b> | <b>{3}</b> printed pages</div>
                        </li>
                    """.format(aux_tonner, tonner['value_name'], tonner['value_percent'], tonner['printed_pages'])
                aux_tonner = """ \
                    {0}
                    </ul>
                """.format(aux_tonner)
                aux_subnet = """\
                {0}
                <tr>
                    <td>{1}</td>
                    <td>{2}</td>
                    <td>{3}</td>
                </tr>
                """.format(aux_subnet, row['number'], row['description'], aux_tonner)
            aux_subnet = """ \
                {0}
                </table>
            """.format(aux_subnet)
            aux = "{0} {1}".format(aux, aux_subnet)

    # inventarios
    inv_temp = []
    for row in inventories:
        inv_temp.append('<h3>{0}</h3>'.format(row['name']))
        inv_temp.append('<br/>')
        inv_temp.append("""
            <table class="subnet">
                <tr>
                    <th>Toner</th>
                    <th>Quantity</th>
                    <th>Backup</th>
                </tr>
            """)
        for toner in row['toners']:
            inv_temp.append("""
            <tr>
                <td>{0}</td>
                <td>{1}</td>
                <td>{2}</td>
            </tr>
            """.format(toner['name'],
                       toner['quantity'], toner['backup_quantity']))
        inv_temp.append('</table>')
        inv_temp.append('<br/>')
    inv_aux1 = ''.join(inv_temp)
    aux = """ \
        {0}
        </div>
        <br />
        <div>
            <h2>Inventories</h2>
            <br />
            <br />
            {1}
        </div>
    </body>
    </html>
    """.format(aux, inv_aux1)
    return aux


def __enumerate():
    """Enumera las impresoras"""
    for item in subnets:
        aux = 1
        for printer in item['printers']:
            printer['number'] = aux
            aux += 1


def __get_info_printer(subnet, printer_ip):
    """Captura los valores de tinta"""
    try:  # 10.32.10.16
        page = requests.get(
            "http://{0}/hp/device/info_suppliesStatus.html?tab=Home&amp;menu=SupplyStatus".format(printer_ip), timeout=2)
        # "http://{0}:8080/test.html".format(printer_ip), timeout=1)  # test
        status_code = page.status_code
    except Exception:
        status_code = 0
    if status_code == 200:
        soup = BeautifulSoup(page.content, 'html.parser')
        table = soup.find_all('table', 'width100')[0]
        toner_low = []
        printer_description = get_value(soup.find_all('div', 'userId')[
            0].get_text())
        # recorre cada color
        for row in table.find_all('table', 'mainContentArea')[:-1]:
            value_name = get_value(row.find_all('td', 'SupplyName width65')[
                0].get_text())  # nombre
            value_percent = get_value(row.find_all('td', 'SupplyName width35 alignRight')[
                0].get_text())  # porcentaje
            values = row.find_all('td', 'itemSpsFont')
            value_printed_pages = 0
            if values:
                value_state = values[0].get_text()  # estado
                if len(values) > 1:  # valor ademas del status
                    # paginas impresas
                    value_printed_pages = values[2].get_text()
            # porcentaje numero
            if '--%' in value_percent:
                value_percent = '0%'
                percent = 0
            else:
                aux = re.search(r'\d+(?=%)', value_percent)
                if aux:
                    percent = aux.group(0)
                else:
                    value_percent = percent = 'unknown'
            if full_report or percent == 'unknown' or int(percent) <= MIN_VALUE_PERCENT:
                toner_low.append({
                    'number': 0,
                    'value_name': value_name,
                    'percent': percent,
                    'printed_pages': value_printed_pages,
                    'value_percent': value_percent,
                    'status': value_state
                })
        if len(toner_low) > 0:
            subnet['printers'].append({
                'printer_ip': printer_ip,
                'description': printer_description,
                'toner_low': toner_low
            })
        # print("{0} end scan".format(printer_ip))


# argumentos en consola
if len(sys.argv) > 1:
    full_report = sys.argv[1].lower() == 'full_tonner_report'
print('full report: ' + str(full_report) + ' day: ' + day_of_week)

get_inventory()
for subnet in subnets:
    for i in range(1, 255):
        ip = '{0}.{1}'.format(subnet['subnet'], i)  # crea ip
        # ip = "localhost"
        tr = threading.Thread(target=__get_info_printer,
                              args=(subnet, ip))
        threads.append(tr)
        tr.start()

# espera todos los hilos
for thread in threads:
    thread.join()


def send_email(report, ccid):
    """Envia correo electronico"""
    doc = __create_html(report)
    if doc:
        msg = MIMEMultipart('alternative')
        msg['Subject'] = 'Toner control'
        msg['From'] = SUPPORT_EMAIL
        msg['To'] = ', '.join(ccid)
        text = "Toner control"
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(doc, 'html')
        # Agrega las partes al contenido del mensaje
        msg.attach(part1)
        msg.attach(part2)
        # Envia mensaje via SMTP
        mail = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        mail.ehlo()
        mail.starttls()
        mail.login(SUPPORT_EMAIL, PASSWORD)
        mail.sendmail(SUPPORT_EMAIL, ccid, msg.as_string())
        mail.quit()


__enumerate()  # enumera las impresoras encontradas
emails = ('jaguilar@dealergeek.com',  # ])
          'flopez@dealergeek.com', 'esolano@dealergeek.com', 'bgriffith@dealergeek.com',
          'wgriffith@dealergeek.com')
emails = ('jaguilar@dealergeek.com', 'flopez@dealergeek.com')
send_email(subnets, emails)
print('terminado')
