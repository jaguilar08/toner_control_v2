from urllib2 import Request, urlopen
import urllib


class DGAPI:
    def send_request(self, type_method, endpoint, json_param):
        url = 'http://173.235.74.153:8015/toner/' + endpoint
        # url = url + '?' + urllib.urlencode(json_param)
        request = Request(url)
        response = urlopen(request)
        return response.read()

    def post(self, method, params):
        return self.send_request('POST', method, params)

    def get(self, method):
        return self.send_request('GET', method, None)


# dg = DGAPI()
# print(dg.get("get_all_carrier"))
